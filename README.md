# IDSHUB

## Descrição
Essa é uma plataforma para registrar os projetos da disciplina de MAC0350 - Introdução ao Desenvolvimento de Sistemas de Software.

## Funcionalidades

### Usuários
Um usuário pode criar um perfil com nome, email, senha e foto (podendo alterar essas informações posteriormente).

Dessa forma, ao logar na sua conta, é possível ver seus grupos, projetos e tutoriais. Porém, também há uma página em que é possível ver todos os projetos e tutoriais que existem (para essa página, não é necessário logar).

Vale lembrar lembrar que não é possível excluir uma conta.

### Grupos
Um usuário pode criar grupos, fazer solicitações para entrar nos grupos, sair de grupos, aceitar/rejeitar solicitações.

Grupos irão possuir além dos usuários membros, uma foto (que pode ser alterada) e conteúdos (projetos e posts).

Assim como os usuários, não é possível excluir os grupos (mas caso não haja membros em um grupo, não é possível solicitar para entrar).

### Projetos
Cada projeto possui um título, uma descrição geral de suas funcionalidades, um link para o repositório com o código e um espaço para críticas e sugestões através de comentários de outros usuários. Além disso os projetos também têm uma indicação do ciclo de desenvolvimento em que estão.

Diferentemente dos usuários e dos grupos, os projetos podem ser excluídos por qualquer um do grupo.

### Posts
Cada post possui um título, uma descrição e o conteúdo de um tutorial. Os posts também permitem que outros usários façam comentários com críticas e sugestões.

Da mesma maneira que os projetos, os posts podem ser excluídos por qualquer um do grupo.

### Comentários

Como descrito acima, usuários podem fazer comentários em projetos e posts. Esses comentários podem receber likes ou dislikes de outros usuários.

Assim como usuários e grupos, não é possível excluídos um comentário (exceto no caso em que o projeto ou post seja excluído).

## Tecnologias usadas

### Backend e Banco de Dados
Para o Backend foi usado o Ktor. Já para o Banco de Dados foi usada a biblioteca Exposed e o H2.

### Frontend
Para o Frontend foi usado HTML, CSS, JavaScript, Bootstrap e React.js.

### Testes
Todas as operações do Banco de Dados foram testadas com a biblioteca kotlin.test.

## Créditos
O IDSHub foi escrito por Felipe Aníbal e Vinícius Lira como um projeto da disciplina MAC0350 - Introdução ao Desenvolvimento de Sistemas de Software - sob a orientação do professor Paulo Meirelles.

## Licença
GNU General Public License v3.0 or later.