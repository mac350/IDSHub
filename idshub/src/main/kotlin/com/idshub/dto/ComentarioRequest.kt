package com.idshub.dto

import com.idshub.models.Comentario
import kotlinx.serialization.Serializable

@Serializable
class ComentarioRequest(
    val idUsuario: Int,
    val idComentarioPai: Int?,
    val tipoConteudo: String,
    val idConteudo: Int,
    val texto: String
){
    fun toComentario() = Comentario(
        idUsuario = idUsuario,
        idComentarioPai = idComentarioPai,
        idConteudo = idConteudo,
        tipoConteudo = tipoConteudo,
        texto = texto
    )
}