package com.idshub.dto

import com.idshub.models.Grupo
import kotlinx.serialization.Serializable

@Serializable
class GrupoRequest(
    val nome: String,
    val idUsuarioCriador: Int,
    val caminhoFoto: String
){
    fun toGrupo() = Grupo(
        nome = nome
    )
}