package com.idshub.dto

import com.idshub.models.Projeto
import kotlinx.serialization.Serializable

@Serializable
class ProjetoRequest(
    val titulo: String,
    val idGrupo: Int,
    val descricao: String,
    val repositorio: String,
    val caminhoFoto: String
) {
    fun toProjeto() = Projeto(
        titulo = titulo,
        idGrupo = idGrupo,
        descricao = descricao,
        repositorio = repositorio
    )
}