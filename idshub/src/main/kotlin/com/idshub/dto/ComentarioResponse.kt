package com.idshub.dto

import com.idshub.models.Comentario
import kotlinx.serialization.Serializable

@Serializable
class ComentarioResponse(
    val id: Int,
    val usuario: String?,
    val idComentarioPai: Int?,
    val tipoConteudo: String,
    val idConteudo: Int,
    val texto: String,
    val dataDeRealizacao: String,
    val qtdLikes: Int,
    val qtdDislikes: Int
)

fun Comentario.toComentarioResponse(usuario: String?) = ComentarioResponse(
    id = id,
    usuario = usuario,
    idComentarioPai = idComentarioPai,
    tipoConteudo = tipoConteudo,
    idConteudo = idConteudo,
    texto = texto,
    dataDeRealizacao = dataDeRealizacao,
    qtdLikes = qtdLikes,
    qtdDislikes = qtdDislikes
)