package com.idshub.dto

import com.idshub.models.Foto
import com.idshub.models.Grupo
import kotlinx.serialization.Serializable

@Serializable
class GrupoResponse(
    val id: String,
    val nome: String,
    val foto: Foto?,
    val qtdMembros: Int
)

fun Grupo.toGrupoResponse(caminho: String?) = GrupoResponse(
    id = id.toString(),
    nome = nome,
    foto = Foto(caminho),
    qtdMembros = qtdMembros
)