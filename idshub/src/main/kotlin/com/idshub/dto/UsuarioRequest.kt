package com.idshub.dto

import com.idshub.models.Usuario
import kotlinx.serialization.Serializable

@Serializable
class UsuarioRequest(
    val nome: String,
    val email: String,
    val senha: String,
    val caminhoFoto: String
){
    fun toUsuario() = Usuario(
        nome = nome,
        senha = senha,
        email = email
    )
}