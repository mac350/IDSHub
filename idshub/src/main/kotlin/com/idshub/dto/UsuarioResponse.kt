package com.idshub.dto

import com.idshub.models.Foto
import com.idshub.models.Usuario
import kotlinx.serialization.Serializable

@Serializable
class UsuarioResponse(
    val id: String,
    val nome: String,
    val senha: String,
    val email: String,
    val foto: Foto?
)

fun Usuario.toUsuarioResponse(caminho: String?) = UsuarioResponse(
    id = id.toString(),
    nome = nome,
    senha = senha,
    email = email,
    foto = Foto(caminho)
)