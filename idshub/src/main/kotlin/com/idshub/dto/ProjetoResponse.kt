package com.idshub.dto

import com.idshub.models.Ciclo
import com.idshub.models.Foto
import com.idshub.models.Projeto
import kotlinx.serialization.Serializable

@Serializable
class ProjetoResponse(
    val id: Int,
    val titulo: String,
    val idGrupo: Int,
    val descricao: String,
    val repositorio: String,
    val dataDeCriacao: String,
    val dataDeModificacao: String,
    val ciclo: Ciclo?,
    val foto: Foto?
)

fun Projeto.toProjetoResponse(ciclo: Ciclo?, caminho: String?) = ProjetoResponse(
    id = id,
    titulo = titulo,
    idGrupo = idGrupo,
    descricao = descricao,
    repositorio = repositorio,
    dataDeCriacao = dataDeCriacao,
    dataDeModificacao = dataDeAlteracao,
    ciclo = ciclo,
    foto = Foto(caminho)
)
