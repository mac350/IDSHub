package com.idshub.dto

import com.idshub.models.Post
import kotlinx.serialization.Serializable

@Serializable
class PostResponse(
    val id: Int,
    val titulo: String,
    val idGrupo: Int,
    val descricao: String,
    val texto: String,
    val dataDeCriacao: String,
    val dataDeModificacao: String
)

fun Post.toPostResponse() = PostResponse(
    id = id,
    titulo = titulo,
    idGrupo = idGrupo,
    descricao = descricao,
    texto = texto,
    dataDeCriacao = dataDeCriacao,
    dataDeModificacao = dataDeAlteracao
)
