package com.idshub.dto

import com.idshub.models.Post
import kotlinx.serialization.Serializable

@Serializable
class PostsRequest(
    val titulo: String,
    val idGrupo: Int,
    val descricao: String,
    val texto: String
) {
    fun toPost() = Post(
        titulo = titulo,
        idGrupo = idGrupo,
        descricao = descricao,
        texto = texto
    )
}