package com.idshub.dao

import com.idshub.database.DatabaseFactory.dbQuery
import com.idshub.models.Dislikes
import org.jetbrains.exposed.sql.SqlExpressionBuilder.eq
import org.jetbrains.exposed.sql.and
import org.jetbrains.exposed.sql.deleteWhere
import org.jetbrains.exposed.sql.insert
import org.jetbrains.exposed.sql.select

class DislikeDao{
    suspend fun darDislike(idUsuario: Int, idComentario: Int) = dbQuery{
        Dislikes.insert{
            it[Dislikes.idUsuario] = idUsuario
            it[Dislikes.idComentario] = idComentario
        }
    }

    suspend fun retirarDislike(idUsuario: Int, idComentario: Int): Boolean = dbQuery{
        Dislikes.deleteWhere{(Dislikes.idUsuario eq idUsuario) and (Dislikes.idComentario eq idComentario)} > 0
    }

    suspend fun deuDislike(idUsuario: Int, idComentario: Int): Boolean = dbQuery{
        Dislikes.select{(Dislikes.idUsuario eq idUsuario) and (Dislikes.idComentario eq idComentario)}.count() > 0
    }
}