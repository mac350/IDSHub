package com.idshub.dao

import com.idshub.database.DatabaseFactory.dbQuery
import com.idshub.models.Comentarios
import com.idshub.models.Fotos
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.SqlExpressionBuilder.eq

class FotoDao{
    suspend fun criaFoto(id: Int, tipo: String, caminho: String) = dbQuery{
        Fotos.insert{
            it[Fotos.id] = id
            it[Fotos.tipo] = tipo
            it[Fotos.caminho] = caminho
        }
    }

    suspend fun achaFoto(id: Int, tipo: String): String? = dbQuery{
        Fotos
            .select{(Fotos.id eq id) and (Fotos.tipo eq tipo)}
            .map{it[Fotos.caminho]}
            .singleOrNull()
    }

    suspend fun alteraFoto(id: Int, tipo:String, caminho: String) = dbQuery{
        Fotos.update({(Fotos.id eq id) and (Fotos.tipo eq tipo)}){
            it[Fotos.caminho] = caminho
        }
    }

    suspend fun deletaFoto(id: Int, tipo: String): Boolean = dbQuery{
        Comentarios.deleteWhere{(Fotos.id eq id) and (Fotos.tipo eq tipo)} > 0
    }
}