package com.idshub.dao

import com.idshub.database.DatabaseFactory.dbQuery
import com.idshub.models.*
import org.jetbrains.exposed.sql.*

class CiclosDao{
    private fun resultRowToCiclo(row: ResultRow) = Ciclo(
        numero = row[Ciclos.numero],
        inicio = row[Ciclos.inicio],
        fim = row[Ciclos.fim]
    )

    suspend fun criaCiclo(idProjeto: Int) = dbQuery{
        Ciclos.insert{
            it[Ciclos.idProjeto] = idProjeto
            it[Ciclos.numero] = 0
            it[Ciclos.inicio] = ""
            it[Ciclos.fim] = ""
        }
    }

    suspend fun achaCiclo(idProjeto: Int): Ciclo? = dbQuery{
        Ciclos
            .select{Ciclos.idProjeto eq idProjeto}
            .map(::resultRowToCiclo)
            .singleOrNull()
    }

    suspend fun alteraCiclo(idProjeto: Int, ciclo: Ciclo) = dbQuery{
        Ciclos.update({Ciclos.idProjeto eq idProjeto}){
            it[Ciclos.numero] = ciclo.numero
            it[Ciclos.inicio] = ciclo.inicio
            it[Ciclos.fim] = ciclo.fim
        }
    }
}