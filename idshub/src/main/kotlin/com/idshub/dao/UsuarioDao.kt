package com.idshub.dao

import com.idshub.database.DatabaseFactory.dbQuery
import com.idshub.models.Usuario
import com.idshub.models.Usuarios
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.and

class UsuarioDao {

    private fun resultRowToUsuario(row: ResultRow) = Usuario(
        id = row[Usuarios.id],
        nome = row[Usuarios.nome],
        email = row[Usuarios.email],
        senha = row[Usuarios.senha]
    )

    suspend fun criaUsuario(usuario: Usuario) = dbQuery{
        val insertStatement = Usuarios.insert{
            it[nome] = usuario.nome
            it[senha] = usuario.senha
            it[email] = usuario.email
        }
        insertStatement.resultedValues?.singleOrNull()?.let(::resultRowToUsuario)
    }

    suspend fun emailDisponivel(email: String): Boolean = dbQuery{
        Usuarios.select{Usuarios.email eq email}.count() <= 0
    }

    suspend fun login(email: String, senha: String): Usuario? = dbQuery {
        Usuarios
            .select{(Usuarios.email eq email) and (Usuarios.senha eq senha)}
            .map(::resultRowToUsuario)
            .singleOrNull()
    }

    suspend fun obtemUsuario(id: Int): Usuario? = dbQuery{
        Usuarios
            .select{Usuarios.id eq id}
            .map(::resultRowToUsuario)
            .singleOrNull()
    }

    suspend fun alteraNome(id: Int, nome: String): Boolean = dbQuery{
        Usuarios.update({Usuarios.id eq id}){
            it[Usuarios.nome] = nome
        } > 0
    }

    suspend fun alteraSenha(id: Int, senha: String): Boolean = dbQuery{
        Usuarios.update({Usuarios.id eq id}){
            it[Usuarios.senha] = senha
        } > 0
    }

    suspend fun alteraEmail(id: Int, email: String): Boolean = dbQuery{
        Usuarios.update({Usuarios.id eq id}){
            it[Usuarios.email] = email
        } > 0
    }
}