package com.idshub.dao

import com.idshub.database.DatabaseFactory.dbQuery
import com.idshub.models.UsuariosGrupos
import org.jetbrains.exposed.sql.SqlExpressionBuilder.eq
import org.jetbrains.exposed.sql.and
import org.jetbrains.exposed.sql.deleteWhere
import org.jetbrains.exposed.sql.insert
import org.jetbrains.exposed.sql.select

class UsuarioGrupoDao{
    suspend fun criaRelacao(idUsuario: Int, idGrupo: Int) = dbQuery{
        UsuariosGrupos.insert{
            it[UsuariosGrupos.idUsuario] = idUsuario
            it[UsuariosGrupos.idGrupo] = idGrupo
        }
    }

    suspend fun achaIdUsuarios(idGrupo: Int): List<Int> = dbQuery{
        UsuariosGrupos
            .select{UsuariosGrupos.idGrupo eq idGrupo}
            .map{it[UsuariosGrupos.idUsuario]}
    }

    suspend fun existeRelacao(idUsuario: Int, idGrupo: Int): Boolean = dbQuery{
        UsuariosGrupos.select{(UsuariosGrupos.idUsuario eq idUsuario) and (UsuariosGrupos.idGrupo eq idGrupo)}.count() > 0
    }

    suspend fun achaIdGrupos(idUsuario: Int): List<Int> = dbQuery{
        UsuariosGrupos
            .select{UsuariosGrupos.idUsuario eq idUsuario}
            .map{it[UsuariosGrupos.idGrupo]}
    }

    suspend fun removeRelacao(idUsuario: Int, idGrupo: Int): Boolean = dbQuery{
        UsuariosGrupos.deleteWhere{(UsuariosGrupos.idUsuario eq idUsuario) and (UsuariosGrupos.idGrupo eq idGrupo)} > 0
    }
}