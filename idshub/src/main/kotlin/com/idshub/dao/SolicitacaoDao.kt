package com.idshub.dao

import com.idshub.database.DatabaseFactory.dbQuery
import com.idshub.models.Solicitacoes
import org.jetbrains.exposed.sql.SqlExpressionBuilder.eq
import org.jetbrains.exposed.sql.deleteWhere
import org.jetbrains.exposed.sql.insert
import org.jetbrains.exposed.sql.select
import org.jetbrains.exposed.sql.and

class SolicitacaoDao{
    suspend fun criaRelacao(idUsuario: Int, idGrupo: Int) = dbQuery{
        Solicitacoes.insert{
            it[Solicitacoes.idUsuario] = idUsuario
            it[Solicitacoes.idGrupo] = idGrupo
        }
    }

    suspend fun achaIdUsuarios(idGrupo: Int): List<Int> = dbQuery{
        Solicitacoes
            .select{Solicitacoes.idGrupo eq idGrupo}
            .map{it[Solicitacoes.idUsuario]}
    }

    suspend fun existeRelacao(idUsuario: Int, idGrupo: Int): Boolean = dbQuery{
        Solicitacoes.select{(Solicitacoes.idUsuario eq idUsuario) and (Solicitacoes.idGrupo eq idGrupo)}.count() > 0
    }

    suspend fun removeRelacao(idUsuario: Int, idGrupo: Int): Boolean = dbQuery{
        Solicitacoes.deleteWhere{(Solicitacoes.idUsuario eq idUsuario) and (Solicitacoes.idGrupo eq idGrupo)} > 0
    }
}