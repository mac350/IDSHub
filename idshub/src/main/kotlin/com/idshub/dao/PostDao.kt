package com.idshub.dao

import com.idshub.database.DatabaseFactory.dbQuery
import com.idshub.models.*
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.SqlExpressionBuilder.eq

class PostDao {
    private fun resultRowToPost(row: ResultRow) = Post(
        id = row[Posts.id],
        titulo = row[Posts.titulo],
        idGrupo = row[Posts.idGrupo],
        descricao = row[Posts.descricao],
        texto = row[Posts.texto],
        dataDeCriacao = row[Posts.dataDeCriacao],
        dataDeAlteracao = row[Posts.dataDeModificacao]
    )

    suspend fun criaPost(post: Post) = dbQuery {
        val insertStatement = Posts.insert{
            it[titulo] = post.titulo
            it[idGrupo] = post.idGrupo
            it[descricao] = post.descricao
            it[texto] = post.texto
            it[dataDeCriacao] = post.dataDeCriacao
            it[dataDeModificacao] = post.dataDeAlteracao
        }
        insertStatement.resultedValues?.singleOrNull()?.let(::resultRowToPost)
    }

    suspend fun obtemPost(id: Int): Post? = dbQuery{
        Posts
            .select{Posts.id eq id}
            .map(::resultRowToPost)
            .singleOrNull()
    }

    suspend fun obtemPosts(): List<Post> = dbQuery{
        Posts.selectAll().map(::resultRowToPost)
    }

    suspend fun postsDoGrupo(idGrupo: Int): List<Post> = dbQuery{
        Posts
            .select{Posts.idGrupo eq idGrupo}
            .map(::resultRowToPost)
    }

    suspend fun alteraTitulo(id: Int, titulo: String) = dbQuery{
        Posts.update({Posts.id eq id}){
            it[Posts.titulo] = titulo
            it[Posts.dataDeModificacao] = dataAtual()
        } > 0
    }

    suspend fun alteraDescricao(id: Int, descricao: String) = dbQuery{
        Posts.update({Posts.id eq id}){
            it[Posts.descricao] = descricao
            it[Posts.dataDeModificacao] = dataAtual()
        } > 0
    }

    suspend fun alteraTexto(id: Int, texto: String) = dbQuery{
        Posts.update({Posts.id eq id}){
            it[Posts.texto] = texto
            it[Posts.dataDeModificacao] = dataAtual()
        } > 0
    }

    suspend fun deletaPost(id: Int): Boolean = dbQuery{
        Posts.deleteWhere{Posts.id eq id} > 0
    }
}