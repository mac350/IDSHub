package com.idshub.dao

import com.idshub.database.DatabaseFactory.dbQuery
import com.idshub.models.Grupo
import com.idshub.models.Grupos
import org.jetbrains.exposed.sql.*

class GrupoDao {
    private fun resultRowGrupo(row: ResultRow) = Grupo(
        id = row[Grupos.id],
        nome = row[Grupos.nome],
        qtdMembros = row[Grupos.qtdMembros]
    )

    suspend fun criaGrupo(grupo: Grupo) = dbQuery{
        val insertStatement = Grupos.insert{
            it[nome] = grupo.nome
            it[qtdMembros] = 1
        }
        insertStatement.resultedValues?.singleOrNull()?.let(::resultRowGrupo)
    }

    suspend fun obtemGrupo(id: Int): Grupo? = dbQuery{
        Grupos
            .select{Grupos.id eq id}
            .map(::resultRowGrupo)
            .singleOrNull()
    }

    suspend fun obtemGrupos(): List<Grupo> = dbQuery{
        Grupos.selectAll().map(::resultRowGrupo)
    }

    suspend fun alteraNome(id: Int, nome: String): Boolean = dbQuery{
        Grupos.update({Grupos.id eq id}){
            it[Grupos.nome] = nome
        } > 0
    }

    suspend fun somaQtdMembros(id: Int) = dbQuery {
        val valor = Grupos.select{ Grupos.id eq id}.map{it[Grupos.qtdMembros]}.singleOrNull()

        if(valor != null)
            Grupos.update({ Grupos.id eq id}){
                it[Grupos.qtdMembros] = valor + 1
            }
    }

    suspend fun subraiQtdMembros(id: Int) = dbQuery {
        val valor = Grupos.select{ Grupos.id eq id}.map{it[Grupos.qtdMembros]}.singleOrNull()

        if(valor != null)
            Grupos.update({ Grupos.id eq id}){
                it[Grupos.qtdMembros] = valor - 1
            }
    }
}