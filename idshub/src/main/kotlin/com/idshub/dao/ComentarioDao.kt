package com.idshub.dao

import com.idshub.database.DatabaseFactory.dbQuery
import com.idshub.models.*
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.SqlExpressionBuilder.eq

class ComentarioDao {
    private fun resultRowComentario(row: ResultRow) = Comentario(
        id = row[Comentarios.id],
        idUsuario = row[Comentarios.idUsuario],
        idComentarioPai = row[Comentarios.idComentarioPai],
        texto = row[Comentarios.texto],
        idConteudo = row[Comentarios.idConteudo],
        tipoConteudo = row[Comentarios.tipoConteudo],
        dataDeRealizacao = row[Comentarios.dataDeRealizacao],
        qtdLikes = row[Comentarios.qtdLikes],
        qtdDislikes = row[Comentarios.qtdDislikes]
    )

    suspend fun criaComentario(comentario: Comentario) = dbQuery{
        val insertStatement = Comentarios.insert{
            it[idUsuario] = comentario.idUsuario
            it[texto] = comentario.texto
            it[idComentarioPai] = comentario.idComentarioPai
            it[idConteudo] = comentario.idConteudo
            it[tipoConteudo] = comentario.tipoConteudo
            it[dataDeRealizacao] = comentario.dataDeRealizacao
            it[qtdLikes] = 0
            it[qtdDislikes] = 0
        }
        insertStatement.resultedValues?.singleOrNull()?.let(::resultRowComentario)
    }

    suspend fun obtemComentariosPai(idConteudo: Int, tipoConteudo: String): List<Comentario> = dbQuery{
        Comentarios
            .select{(Comentarios.idComentarioPai eq null) and (Comentarios.idConteudo eq idConteudo) and (Comentarios.tipoConteudo eq tipoConteudo)}
            .map(::resultRowComentario)
    }

    suspend fun obtemComentariosFilho(idConteudo: Int, idPai: Int?, tipoConteudo: String): List<Comentario> = dbQuery{
        Comentarios
            .select{(Comentarios.idComentarioPai eq idPai) and (Comentarios.idConteudo eq idConteudo) and (Comentarios.tipoConteudo eq tipoConteudo)}
            .map(::resultRowComentario)
    }

    suspend fun deletaComentarios(idConteudo: Int, tipoConteudo: String): Boolean = dbQuery{
        Comentarios.deleteWhere{(Comentarios.idConteudo eq idConteudo) and (Comentarios.tipoConteudo eq tipoConteudo)} > 0
    }

    suspend fun somaLike(id: Int) = dbQuery{
        val valor = Comentarios.select{Comentarios.id eq id}.map{it[Comentarios.qtdLikes]}.singleOrNull()
        if(valor != null)
            Comentarios.update({Comentarios.id eq id}){
                it[Comentarios.qtdLikes] = valor + 1
            }
    }

    suspend fun somaDislike(id: Int) = dbQuery {
        val valor = Comentarios.select{Comentarios.id eq id}.map{it[Comentarios.qtdDislikes]}.singleOrNull()

        if(valor != null)
            Comentarios.update({Comentarios.id eq id}){
                it[Comentarios.qtdDislikes] = valor + 1
            }
    }

    suspend fun subtraiLike(id: Int) = dbQuery {
        val valor = Comentarios.select{Comentarios.id eq id}.map{it[Comentarios.qtdLikes]}.singleOrNull()
        if(valor != null)
            Comentarios.update({Comentarios.id eq id}){
                it[Comentarios.qtdLikes] = valor - 1
            }
    }

    suspend fun subtraiDislike(id: Int) = dbQuery {
        val valor = Comentarios.select{Comentarios.id eq id}.map{it[Comentarios.qtdDislikes]}.singleOrNull()
        if(valor != null)
            Comentarios.update({Comentarios.id eq id}){
                it[Comentarios.qtdDislikes] = valor - 1
            }
    }
}