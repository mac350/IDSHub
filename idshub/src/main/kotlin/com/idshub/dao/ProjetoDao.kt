package com.idshub.dao

import com.idshub.database.DatabaseFactory.dbQuery
import com.idshub.models.*
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.SqlExpressionBuilder.eq

class ProjetoDao {
    private fun resultRowToProjeto(row: ResultRow) = Projeto(
        id = row[Projetos.id],
        titulo = row[Projetos.titulo],
        idGrupo = row[Projetos.idGrupo],
        descricao = row[Projetos.descricao],
        repositorio = row[Projetos.repositorio],
        dataDeCriacao = row[Projetos.dataDeCriacao],
        dataDeAlteracao = row[Projetos.dataDeModificacao]
    )

    suspend fun obtemProjetos(): List<Projeto> = dbQuery{
        Projetos.selectAll().map(::resultRowToProjeto)
    }

    suspend fun obtemProjeto(id: Int): Projeto? = dbQuery{
        Projetos
            .select { Projetos.id eq id }
            .map(::resultRowToProjeto)
            .singleOrNull()
    }

    suspend fun projetosDoGrupo(idGrupo: Int): List<Projeto> = dbQuery{
        Projetos
            .select{Projetos.idGrupo eq idGrupo}
            .map(::resultRowToProjeto)
    }

    suspend fun criaProjeto(projeto: Projeto) = dbQuery{
        val insertStatement = Projetos.insert{
            it[titulo] = projeto.titulo
            it[idGrupo] = projeto.idGrupo
            it[descricao] = projeto.descricao
            it[repositorio] = projeto.repositorio
            it[dataDeCriacao] = projeto.dataDeCriacao
            it[dataDeModificacao] = projeto.dataDeAlteracao
        }
        insertStatement.resultedValues?.singleOrNull()?.let(::resultRowToProjeto)
    }

    suspend fun alteraTitulo(id: Int, titulo: String) = dbQuery{
        Projetos.update({Projetos.id eq id }){
            it[Projetos.titulo] = titulo
            it[Projetos.dataDeModificacao] = dataAtual()
        } > 0
    }

    suspend fun alteraDescricao(id: Int, descricao: String) = dbQuery{
        Projetos.update({Projetos.id eq id}){
            it[Projetos.descricao] = descricao
            it[Projetos.dataDeModificacao] = dataAtual()
        } > 0
    }

    suspend fun alteraRepositorio(id: Int, repositorio: String) = dbQuery{
        Projetos.update({Projetos.id eq id}){
            it[Projetos.repositorio] = repositorio
            it[Projetos.dataDeModificacao] = dataAtual()
        } > 0
    }

    suspend fun deletaProjeto(id: Int): Boolean = dbQuery{
        Projetos.deleteWhere{Projetos.id eq id} > 0
    }
}