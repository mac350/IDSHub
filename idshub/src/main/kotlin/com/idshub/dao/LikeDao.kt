package com.idshub.dao

import com.idshub.database.DatabaseFactory.dbQuery
import com.idshub.models.Likes
import org.jetbrains.exposed.sql.SqlExpressionBuilder.eq
import org.jetbrains.exposed.sql.and
import org.jetbrains.exposed.sql.deleteWhere
import org.jetbrains.exposed.sql.insert
import org.jetbrains.exposed.sql.select

class LikeDao{
    suspend fun darLike(idUsuario: Int, idComentario: Int) = dbQuery{
        Likes.insert{
            it[Likes.idUsuario] = idUsuario
            it[Likes.idComentario] = idComentario
        }
    }

    suspend fun retirarLike(idUsuario: Int, idComentario: Int): Boolean = dbQuery{
        Likes.deleteWhere{(Likes.idUsuario eq idUsuario) and (Likes.idComentario eq idComentario)} > 0
    }

    suspend fun deuLike(idUsuario: Int, idComentario: Int): Boolean = dbQuery{
        Likes.select{(Likes.idUsuario eq idUsuario) and (Likes.idComentario eq idComentario)}.count() > 0
    }
}