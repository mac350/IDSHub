package com.idshub.models

import org.jetbrains.exposed.sql.*
import java.text.SimpleDateFormat
import java.util.Date

val dateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
fun dataAtual(): String = dateFormat.format(Date())

abstract class Conteudo(
    var titulo: String,
    var descricao: String,
    val id: Int = 0,
    val idGrupo: Int,
    val dataDeCriacao: String = dataAtual(),
    var dataDeAlteracao: String = dataDeCriacao
)

class Post(
    titulo: String,
    descricao: String,
    id: Int = 0,
    idGrupo: Int,
    var texto: String = "",
    dataDeCriacao: String = dataAtual(),
    dataDeAlteracao: String = dataDeCriacao
): Conteudo(
    titulo = titulo,
    idGrupo = idGrupo,
    descricao = descricao,
    id = id,
    dataDeCriacao = dataDeCriacao,
    dataDeAlteracao = dataDeAlteracao
)

class Projeto(
    titulo: String,
    descricao: String,
    id: Int = 0,
    idGrupo: Int,
    dataDeCriacao: String = dataAtual(),
    dataDeAlteracao: String = dataDeCriacao,
    var repositorio: String = ""
): Conteudo(
    titulo = titulo,
    descricao = descricao,
    id = id, idGrupo = idGrupo,
    dataDeAlteracao = dataDeAlteracao,
    dataDeCriacao = dataDeCriacao
)

object Posts : Table() {
    val id = integer("id").autoIncrement()
    val titulo = text("titulo")
    val idGrupo = integer("idGrupo")
    val descricao = text("descricao")
    val texto = text("texto")
    val dataDeCriacao = text("dataDeCriacao")
    val dataDeModificacao = text("dataDeModificacao")

    override val primaryKey = PrimaryKey(id)
}

object Projetos : Table() {
    val id = integer("id").autoIncrement()
    val titulo = text("titulo")
    val idGrupo = integer("idGrupo")
    val descricao = text("descricao")
    val repositorio = text("repositorio")
    val dataDeCriacao = text("dataDeCriacao")
    val dataDeModificacao = text("dataDeModificacao")

    override val primaryKey = PrimaryKey(id)
}