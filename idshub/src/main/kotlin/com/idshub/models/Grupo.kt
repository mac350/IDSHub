package com.idshub.models

import org.jetbrains.exposed.sql.Table

class Grupo(
    var nome : String,
    val id: Int = 0,
    val qtdMembros: Int = 1
)

object Grupos: Table(){
    val id = integer("id").autoIncrement()
    val nome = text("nome")
    val qtdMembros = integer("qtdMembros")

    override val primaryKey = PrimaryKey(id)
}

object Solicitacoes: Table(){
    val idUsuario = integer("idUsuario")
    val idGrupo = integer("idGrupo")

    override val primaryKey = PrimaryKey(idUsuario, idGrupo)
}