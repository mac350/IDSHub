package com.idshub.models

import kotlinx.serialization.Serializable
import org.jetbrains.exposed.sql.ReferenceOption
import org.jetbrains.exposed.sql.Table

@Serializable
class Ciclo(
    var numero: Int = 0,
    var inicio: String = "",
    var fim: String = ""
)

object Ciclos: Table(){
    val idProjeto = integer("idProjeto").references(Projetos.id, onDelete = ReferenceOption.CASCADE)
    val numero = integer("numero")
    val inicio = text("inicio")
    val fim = text("fim")

    override val primaryKey = PrimaryKey(Ciclos.idProjeto)
}
