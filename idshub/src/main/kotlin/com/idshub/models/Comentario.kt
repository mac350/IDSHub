package com.idshub.models

import org.jetbrains.exposed.sql.ReferenceOption
import org.jetbrains.exposed.sql.Table

class Comentario(
    val idUsuario: Int,
    val id: Int = 0,
    val texto: String,
    val idConteudo: Int,
    val tipoConteudo: String,
    val dataDeRealizacao: String = dataAtual(),
    val idComentarioPai: Int? = null,
    val qtdLikes: Int = 0,
    val qtdDislikes: Int = 0
)

object Comentarios : Table() {
    val id = integer("id").autoIncrement()
    val idUsuario = integer("idUsuario")
    val idComentarioPai = integer("idComentarioPai").nullable()
    val tipoConteudo = text("tipoConteudo")
    val idConteudo = integer("idConteudo")
    val texto = text("texto")
    val dataDeRealizacao = text("dataDeRealizacao")
    val qtdLikes = integer("qtdLikes")
    val qtdDislikes = integer("qtdDislikes")

    override val primaryKey = PrimaryKey(id)
}

object Likes: Table(){
    val idComentario = integer("idComentario").references(Comentarios.id, onDelete = ReferenceOption.CASCADE)
    val idUsuario = integer("idUsuario")

    override val primaryKey = PrimaryKey(idComentario, idUsuario)
}

object Dislikes: Table(){
    val idComentario = integer("idComentario").references(Comentarios.id, onDelete = ReferenceOption.CASCADE)
    val idUsuario = integer("idUsuario")

    override val primaryKey = PrimaryKey(idComentario, idUsuario)
}