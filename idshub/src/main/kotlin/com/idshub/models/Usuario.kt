package com.idshub.models

import kotlinx.serialization.Serializable
import org.jetbrains.exposed.sql.*

class Usuario(
    var nome: String,
    var senha: String,
    val id: Int = 0,
    val email: String
)

object Usuarios: Table(){
    val id = integer("id").autoIncrement()
    val senha = text("senha")
    val email = text("email")
    val nome = text("nome")

    override val primaryKey = PrimaryKey(id)
}

object UsuariosGrupos: Table(){
    val idUsuario = integer("idUsuario")
    val idGrupo = integer("idGrupo")

    override val primaryKey = PrimaryKey(idUsuario, idGrupo)
}

// para solitações e relações de um usuário pertencer a um grupo
@Serializable
data class UsuarioGrupo(val idUsuario: Int, val idGrupo: Int)

// para likes/dislikes
@Serializable
data class UsuarioComentario(val idUsuario: Int, val idComentario: Int)