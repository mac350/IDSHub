package com.idshub.models

import kotlinx.serialization.Serializable
import org.jetbrains.exposed.sql.Table

@Serializable
class Foto(
    var caminho: String? = ""
)

object Fotos: Table(){
    val id = integer("id")
    val tipo = text("tipo")
    val caminho = text("caminho")

    override val primaryKey = PrimaryKey(Fotos.id, Fotos.tipo)
}