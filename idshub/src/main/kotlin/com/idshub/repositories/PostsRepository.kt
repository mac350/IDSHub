package com.idshub.repositories

import com.idshub.dao.PostDao
import com.idshub.models.Post

class PostsRepository(
    private val dao: PostDao = PostDao()
){
    suspend fun criaPost(post: Post) = dao.criaPost(post)
    suspend fun obtemPost(id: Int) = dao.obtemPost(id)
    suspend fun obtemPosts() = dao.obtemPosts()
    suspend fun postsDoGrupo(idGrupo: Int) = dao.postsDoGrupo(idGrupo)
    suspend fun alteraTitulo(id: Int, titulo: String) = dao.alteraTitulo(id, titulo)
    suspend fun alteraDescricao(id: Int, descricao: String) = dao.alteraDescricao(id, descricao)
    suspend fun alteraTexto(id: Int, texto: String) = dao.alteraTexto(id, texto)
    suspend fun deletaPost(id: Int) = dao.deletaPost(id)
}