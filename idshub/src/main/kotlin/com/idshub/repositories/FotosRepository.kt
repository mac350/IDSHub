package com.idshub.repositories

import com.idshub.dao.FotoDao

class FotosRepository(
    private val dao: FotoDao = FotoDao()
){
    suspend fun criaFoto(id: Int, tipo: String, caminho: String) = dao.criaFoto(id, tipo, caminho)
    suspend fun achaFoto(id: Int, tipo: String) = dao.achaFoto(id, tipo)
    suspend fun alteraFoto(id: Int, tipo: String, caminho: String) = dao.alteraFoto(id, tipo, caminho)
    suspend fun deletaFoto(id: Int, tipo: String) = dao.deletaFoto(id, tipo)
}