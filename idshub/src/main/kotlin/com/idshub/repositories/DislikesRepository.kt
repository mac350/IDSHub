package com.idshub.repositories

import com.idshub.dao.DislikeDao

class DislikesRepository(
    private val dao: DislikeDao = DislikeDao()
){
    suspend fun darDislike(idUsuario: Int, idComentario: Int) = dao.darDislike(idUsuario, idComentario)
    suspend fun retirarDislike(idUsuario: Int, idComentario: Int) = dao.retirarDislike(idUsuario, idComentario)
    suspend fun deuDislike(idUsuario: Int, idComentario: Int) = dao.deuDislike(idUsuario, idComentario)
}