package com.idshub.repositories

import com.idshub.dao.ProjetoDao
import com.idshub.models.Projeto

class ProjetosRepository(
    private val dao: ProjetoDao = ProjetoDao()
) {
    suspend fun criaProjeto(projeto: Projeto) = dao.criaProjeto(projeto)
    suspend fun obtemProjeto(id: Int) = dao.obtemProjeto(id)
    suspend fun obtemProjetos() = dao.obtemProjetos()
    suspend fun projetosDoGrupo(idGrupo: Int) = dao.projetosDoGrupo(idGrupo)
    suspend fun alteraTitulo(id: Int, titulo: String) = dao.alteraTitulo(id, titulo)
    suspend fun alteraDescricao(id: Int, descricao: String) = dao.alteraDescricao(id, descricao)
    suspend fun alteraRepositorio(id: Int, texto: String) = dao.alteraRepositorio(id, texto)
    suspend fun deletaProjeto(id: Int) = dao.deletaProjeto(id)
}