package com.idshub.repositories

import com.idshub.dao.UsuarioGrupoDao

class UsuariosGruposRepository(
    private val dao: UsuarioGrupoDao = UsuarioGrupoDao()
) {
    suspend fun criaRelacao(idUsuario: Int, idGrupo: Int) = dao.criaRelacao(idUsuario, idGrupo)
    suspend fun achaIdGrupos(idUsuario: Int) = dao.achaIdGrupos(idUsuario)
    suspend fun existeRelacao(idUsuario: Int, idGrupo: Int) = dao.existeRelacao(idUsuario, idGrupo)
    suspend fun achaIdUsuarios(idGrupo: Int) = dao.achaIdUsuarios(idGrupo)
    suspend fun removeRelacao(idUsuario: Int, idGrupo: Int) = dao.removeRelacao(idUsuario, idGrupo)
}