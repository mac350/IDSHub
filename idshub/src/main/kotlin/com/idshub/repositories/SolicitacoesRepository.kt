package com.idshub.repositories

import com.idshub.dao.SolicitacaoDao

class SolicitacoesRepository(
    private val dao: SolicitacaoDao = SolicitacaoDao()
){
    suspend fun criaRelacao(idUsuario: Int, idGrupo: Int) = dao.criaRelacao(idUsuario, idGrupo)
    suspend fun achaIdUsuarios(idGrupo: Int) = dao.achaIdUsuarios(idGrupo)
    suspend fun existeRelacao(idUsuario: Int, idGrupo: Int) = dao.existeRelacao(idUsuario, idGrupo)
    suspend fun removeRelacao(idUsuario: Int, idGrupo: Int) = dao.removeRelacao(idUsuario, idGrupo)
}