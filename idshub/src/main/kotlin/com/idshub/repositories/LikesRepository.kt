package com.idshub.repositories

import com.idshub.dao.LikeDao

class LikesRepository(
    private val dao: LikeDao = LikeDao()
){
    suspend fun darLike(idUsuario: Int, idComentario: Int) = dao.darLike(idUsuario, idComentario)
    suspend fun retirarLike(idUsuario: Int, idComentario: Int) = dao.retirarLike(idUsuario, idComentario)
    suspend fun deuLike(idUsuario: Int, idComentario: Int) = dao.deuLike(idUsuario, idComentario)
}