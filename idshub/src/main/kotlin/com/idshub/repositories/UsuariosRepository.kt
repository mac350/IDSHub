package com.idshub.repositories

import com.idshub.dao.UsuarioDao
import com.idshub.models.Usuario

class UsuariosRepository(
    private val dao: UsuarioDao = UsuarioDao()
){
    suspend fun criaUsuario(usuario: Usuario) = dao.criaUsuario(usuario)
    suspend fun emailDisponivel(email: String) = dao.emailDisponivel(email)
    suspend fun login(email: String, senha: String) = dao.login(email, senha)
    suspend fun obtemUsuario(id: Int) = dao.obtemUsuario(id)
    suspend fun alteraNome(id: Int, nome: String) = dao.alteraNome(id, nome)
    suspend fun alteraEmail(id: Int, email: String) = dao.alteraEmail(id, email)
    suspend fun alteraSenha(id: Int, senha: String) = dao.alteraSenha(id, senha)
}