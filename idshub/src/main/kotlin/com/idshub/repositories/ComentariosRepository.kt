package com.idshub.repositories

import com.idshub.dao.ComentarioDao
import com.idshub.models.Comentario

class ComentariosRepository(
    private val dao: ComentarioDao = ComentarioDao()
) {

    suspend fun obtemComentariosPai(idConteudo: Int, tipoConteudo: String) = dao.obtemComentariosPai(idConteudo, tipoConteudo)
    suspend fun obtemComentariosFilho(idConteudo: Int, idPai: Int?, tipoConteudo: String) = dao.obtemComentariosFilho(idConteudo, idPai, tipoConteudo)
    suspend fun criaComentario(comentario: Comentario) = dao.criaComentario(comentario)
    suspend fun deletaComentarios(idConteudo: Int, tipoConteudo: String) = dao.deletaComentarios(idConteudo, tipoConteudo)
    suspend fun somaLike(id: Int) = dao.somaLike(id)
    suspend fun somaDislike(id: Int) = dao.somaDislike(id)
    suspend fun subtraiLike(id: Int) = dao.subtraiLike(id)
    suspend fun subtraiDislike(id: Int) = dao.subtraiDislike(id)
}