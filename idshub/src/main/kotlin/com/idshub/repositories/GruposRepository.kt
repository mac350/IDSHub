package com.idshub.repositories

import com.idshub.dao.GrupoDao
import com.idshub.models.Grupo

class GruposRepository(
    private val dao: GrupoDao = GrupoDao()
){
    suspend fun criaGrupo(grupo: Grupo) = dao.criaGrupo(grupo)
    suspend fun obtemGrupo(id: Int) = dao.obtemGrupo(id)
    suspend fun obtemGrupos() = dao.obtemGrupos()
    suspend fun alteraNome(id: Int, nome: String) = dao.alteraNome(id, nome)
    suspend fun somaQtdMembros(id: Int) = dao.somaQtdMembros(id)
    suspend fun subtraiQtdMembros(id: Int) = dao.subraiQtdMembros(id)
}