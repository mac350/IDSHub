package com.idshub.repositories

import com.idshub.dao.CiclosDao
import com.idshub.models.Ciclo

class CiclosRepository(
    private val dao: CiclosDao = CiclosDao()
){
    suspend fun criaCiclo(idProjeto: Int) = dao.criaCiclo(idProjeto)
    suspend fun alteraCiclo(idProjeto: Int, ciclo: Ciclo) =
        dao.alteraCiclo(idProjeto, ciclo)
    suspend fun achaCiclo(idProjeto: Int) = dao.achaCiclo(idProjeto)
}