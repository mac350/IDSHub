package com.idshub.database

import com.idshub.models.*
import kotlinx.coroutines.*
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.transactions.*
import org.jetbrains.exposed.sql.transactions.experimental.*

object DatabaseFactory{
    fun init(
        driverClassName: String = "org.h2.Driver",
        jdbcURL: String = "jdbc:h2:file:./build/db"
    ){
        val database = Database.connect(jdbcURL, driverClassName)
        transaction(database) {
            SchemaUtils.create(
                Usuarios,
                Grupos,
                UsuariosGrupos,
                Solicitacoes,
                Posts,
                Projetos,
                Comentarios,
                Fotos,
                Ciclos,
                Likes,
                Dislikes
            )
        }
    }

    suspend fun <T> dbQuery(block: suspend () -> T): T =
        newSuspendedTransaction(Dispatchers.IO) { block() }
}