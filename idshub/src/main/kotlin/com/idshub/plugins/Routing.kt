package com.idshub.plugins

import com.idshub.dto.*
import com.idshub.models.Ciclo
import com.idshub.models.UsuarioComentario
import com.idshub.models.UsuarioGrupo
import com.idshub.repositories.*
import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import io.ktor.server.util.*
import kotlinx.serialization.Serializable
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json

fun Application.configureRouting() {

    val usuariosRepository = UsuariosRepository()
    val gruposRepository = GruposRepository()
    val usuariosGruposRepository = UsuariosGruposRepository()
    val solicitacoesRepository = SolicitacoesRepository()
    val postsRepository = PostsRepository()
    val projetosRepository = ProjetosRepository()
    val comentariosRepository = ComentariosRepository()
    val fotosRepository = FotosRepository()
    val ciclosRepository = CiclosRepository()
    val likesRepository = LikesRepository()
    val dislikesRepository = DislikesRepository()

    routing {

        // obtém um usuário a partir de um id
        get("/usuario/{id}"){
            val id = call.parameters.getOrFail<String>("id").toInt()

            val response = usuariosRepository.obtemUsuario(id)
            if(response != null)
                call.respond(response.toUsuarioResponse(fotosRepository.achaFoto(id, "usuario")))
        }

        // adiciona um novo usuário
        post("/criarUsuario"){
            val request = call.receive<UsuarioRequest>()

            if(!usuariosRepository.emailDisponivel(request.email))
                call.respond(-1)

            else{
                val usuario = usuariosRepository.criaUsuario(request.toUsuario())

                if(usuario != null)
                    // adiciona foto
                    fotosRepository.criaFoto(usuario.id, "usuario", request.caminhoFoto)

                call.respond(HttpStatusCode.Created)
            }
        }

        // altera um atributo do usuário
        put("/usuario/{id}"){
            val id = call.parameters.getOrFail<String>("id").toInt()
            val atributo = call.parameters["atributo"]
            val novoValor = call.receive<String>()

            when(atributo){
                "nome" -> usuariosRepository.alteraNome(id, novoValor)
                "senha" -> usuariosRepository.alteraSenha(id, novoValor)
                "email" -> usuariosRepository.alteraEmail(id, novoValor)
                "foto" -> fotosRepository.alteraFoto(id, "usuario", novoValor)
            }

            call.respond(HttpStatusCode.Created)
        }

        // obtém todos os grupos de um usuário
        get("/usuario/{id}/grupos"){
            val id = call.parameters.getOrFail<String>("id").toInt()
            val response = usuariosGruposRepository.achaIdGrupos(id)
                .map{gruposRepository.obtemGrupo(it)?.toGrupoResponse(fotosRepository.achaFoto(it, "grupo"))}
            call.respond(response)
        }

        // obtém todos os projetos de um usuário
        get("/usuario/{id}/projetos"){
            val id = call.parameters.getOrFail<String>("id").toInt()
            val grupos = usuariosGruposRepository.achaIdGrupos(id)
            val response = grupos.flatMap{grupo -> projetosRepository.projetosDoGrupo(grupo)}
                .map{it.toProjetoResponse(ciclosRepository.achaCiclo(it.id), fotosRepository.achaFoto(it.id, "projeto"))}
            call.respond(response)
        }

        // obtém todos os posts de um usuário
        get("/usuario/{id}/posts"){
            val id = call.parameters.getOrFail<String>("id").toInt()
            val grupos = usuariosGruposRepository.achaIdGrupos(id)
            val response = grupos.flatMap{grupo -> postsRepository.postsDoGrupo(grupo)}
                .map {it.toPostResponse()}
            call.respond(response)
        }

        // sair de um grupo
        post("/sairGrupo"){
            val params = call.receive<UsuarioGrupo>()
            val idUsuario = params.idUsuario
            val idGrupo = params.idGrupo

            // sai do grupo
            usuariosGruposRepository.removeRelacao(idUsuario, idGrupo)

            // atualiza a quantidade de membros
            gruposRepository.subtraiQtdMembros(idGrupo)

            call.respond(HttpStatusCode.Created)
        }

        // dar like em um comentário
        post("/darLike"){
            val params = call.receive<UsuarioComentario>()
            val idUsuario = params.idUsuario
            val idComentario = params.idComentario

            // se o usuário tinha dado dislike, tira
            if(dislikesRepository.deuDislike(idUsuario, idComentario)){
                dislikesRepository.retirarDislike(idUsuario, idComentario)
                comentariosRepository.subtraiDislike(idComentario)
            }

            // se o usuário não tinha dado like, dá
            if(!likesRepository.deuLike(idUsuario, idComentario)){
                likesRepository.darLike(idUsuario, idComentario)
                comentariosRepository.somaLike(idComentario)
            }

            call.respond(HttpStatusCode.Created)
        }

        // retirar like em um comentário
        post("/retirarLike"){
            val params = call.receive<UsuarioComentario>()
            val idUsuario = params.idUsuario
            val idComentario = params.idComentario

            // verifica se o usuário tinha dado like
            if(likesRepository.deuLike(idUsuario, idComentario)){
                likesRepository.retirarLike(idUsuario, idComentario)
                comentariosRepository.subtraiLike(idComentario)
            }

            call.respond(HttpStatusCode.Created)
        }

        // dar dislike em um comentário
        post("/darDislike") {
            val params = call.receive<UsuarioComentario>()
            val idUsuario = params.idUsuario
            val idComentario = params.idComentario

            // se o usuário tinha dado like, tira
            if(likesRepository.deuLike(idUsuario, idComentario)){
                likesRepository.retirarLike(idUsuario, idComentario)
                comentariosRepository.subtraiLike(idComentario)
            }

            // se o usuário não tinha dado dislike, dá
            if(!dislikesRepository.deuDislike(idUsuario, idComentario)){
                dislikesRepository.darDislike(idUsuario, idComentario)
                comentariosRepository.somaDislike(idComentario)
            }

            call.respond(HttpStatusCode.Created)
        }

        // retirar dislike em um comentário
        post("/retirarDislike"){
            val params = call.receive<UsuarioComentario>()
            val idUsuario = params.idUsuario
            val idComentario = params.idComentario

            // verifica se o usuário tinha dado dislike
            if(dislikesRepository.deuDislike(idUsuario, idComentario)){
                dislikesRepository.retirarDislike(idUsuario, idComentario)
                comentariosRepository.subtraiDislike(idComentario)
            }

            call.respond(HttpStatusCode.Created)
        }

        // ---------- parte dos grupos ----------

        // adiciona um novo grupo
        post("/grupos"){
            val request = call.receive<GrupoRequest>()
            val grupo = gruposRepository.criaGrupo(request.toGrupo())

            if(grupo != null){
                // cria uma relação entre o usuário que criou o grupo e esse grupo
                usuariosGruposRepository.criaRelacao(request.idUsuarioCriador, grupo.id)

                // adiciona a foto
                fotosRepository.criaFoto(grupo.id, "grupo", request.caminhoFoto)
            }

            call.respond(HttpStatusCode.Created)
        }

        // obtém todos os grupos
        get("/grupos"){
            val response = gruposRepository.obtemGrupos()
                .map {it.toGrupoResponse(fotosRepository.achaFoto(it.id, "grupo"))}
            call.respond(response)
        }

        // obtém um grupo a partir de um id
        get("/grupo/{id}"){
            val id = call.parameters.getOrFail<String>("id").toInt()
            val response = gruposRepository.obtemGrupo(id)
            if(response != null)
                call.respond(response.toGrupoResponse(fotosRepository.achaFoto(id, "grupo")))
        }

        // altera um atributo do grupo
        put("/grupo/{id}"){
            val id = call.parameters.getOrFail<String>("id").toInt()
            val atributo = call.parameters["atributo"]
            val novoValor = call.receive<String>()

            when(atributo){
                "nome" -> gruposRepository.alteraNome(id, novoValor)
                "foto" -> fotosRepository.alteraFoto(id, "grupo", novoValor)
            }

            call.respond(HttpStatusCode.Created)
        }

        // obtém todos os usuários de um grupo
        get("/grupo/{id}/usuarios"){
            val id = call.parameters.getOrFail<String>("id").toInt()
            val response = usuariosGruposRepository.achaIdUsuarios(id)
                .map{usuariosRepository.obtemUsuario(it)?.toUsuarioResponse(fotosRepository.achaFoto(id, "usuario"))}
            call.respond(response)
        }

        // obtém todas os usuários que solicitaram entrar no grupo
        get("grupo/{id}/solicitacoes"){
            val id = call.parameters.getOrFail<String>("id").toInt()
            val response = solicitacoesRepository.achaIdUsuarios(id)
                .map{usuariosRepository.obtemUsuario(it)?.toUsuarioResponse(fotosRepository.achaFoto(id, "usuario"))}
            call.respond(response)
        }

        // obtém todos os posts de um grupo
        get("/grupo/{id}/posts"){
            val id = call.parameters.getOrFail<String>("id").toInt()
            val response = postsRepository.postsDoGrupo(id)
                .map{it.toPostResponse()}
            call.respond(response)
        }

        // obtém todos os projetos de um grupo
        get("/grupo/{id}/projetos"){
            val id = call.parameters.getOrFail<String>("id").toInt()
            val response = projetosRepository.projetosDoGrupo(id)
                .map{it.toProjetoResponse(ciclosRepository.achaCiclo((id)), fotosRepository.achaFoto(id, "projeto"))}
            call.respond(response)
        }

        // ---------- parte para solicitações e adicionar usuários ----------

        // faz solicitação para entrar no grupo
        post("/fazerSolicitacao"){
            val params = call.receive<UsuarioGrupo>()
            val idUsuario = params.idUsuario
            val idGrupo = params.idGrupo

            // se o usuário já não pertence ou já não fez a solicitação
            if((!solicitacoesRepository.existeRelacao(idUsuario, idGrupo)) &&
                !usuariosGruposRepository.existeRelacao(idUsuario, idGrupo)){
                solicitacoesRepository.criaRelacao(idUsuario, idGrupo)
                call.respond(HttpStatusCode.Created)
            }

            call.respond(HttpStatusCode.Created)
        }

        // aceitar solicitação para entrar no grupo
        post("/aceitarSolicitacao"){
            val params = call.receive<UsuarioGrupo>()
            val idUsuario = params.idUsuario
            val idGrupo = params.idGrupo

            // remove das solicitações
            solicitacoesRepository.removeRelacao(idUsuario, idGrupo)

            // entra no grupo
            usuariosGruposRepository.criaRelacao(idUsuario, idGrupo)

            // altera a quantidade de membros
            gruposRepository.somaQtdMembros(idGrupo)

            call.respond(HttpStatusCode.Created)
        }

        // rejeitar solicitação feita para entrar no grupo
        post("/rejeitarSolicitacao"){
            val params = call.receive<UsuarioGrupo>()
            val idUsuario = params.idUsuario
            val idGrupo = params.idGrupo

            // somente remove das solicitações
            solicitacoesRepository.removeRelacao(idUsuario, idGrupo)

            call.respond(HttpStatusCode.Created)
        }

        // adicionar um usuário no grupo
        post("/adicionarUsuario"){
            val params = call.receive<UsuarioGrupo>()
            val idUsuario = params.idUsuario
            val idGrupo = params.idGrupo

            // adiciona no grupo
            usuariosGruposRepository.criaRelacao(idUsuario, idGrupo)

            // altera a quantidade de membros
            gruposRepository.somaQtdMembros(idGrupo)

            call.respond(HttpStatusCode.Created)
        }

        // ---------- parte dos posts ----------

        // adiciona um novo post
        post("/posts"){
            val request = call.receive<PostsRequest>()
            val post = postsRepository.criaPost(request.toPost())
            if(post != null)
                call.respond(post.toPostResponse())
        }

        // obtém um post a partir de um id
        get("/post/{id}"){
            val id = call.parameters.getOrFail<String>("id").toInt()
            val response = postsRepository.obtemPost(id)
            if(response != null)
                call.respond(response.toPostResponse())
        }

        // obtém todos os posts
        get("/posts"){
            val response = postsRepository.obtemPosts()
                .map{it.toPostResponse()}
            call.respond(response)
        }

        // altera um atributo do post
        put("/post/{id}") {
            val id = call.parameters.getOrFail<String>("id").toInt()
            val atributo = call.parameters["atributo"]
            val novoValor = call.receive<String>()

            when(atributo){
                "titulo" -> postsRepository.alteraTitulo(id, novoValor)
                "descricao" -> postsRepository.alteraDescricao(id, novoValor)
                "texto" -> postsRepository.alteraTexto(id, novoValor)
            }

            call.respond(HttpStatusCode.Created)
        }

        // deleta o post
        delete("/post/{id}"){
            val id = call.parameters.getOrFail<String>("id").toInt()
            postsRepository.deletaPost(id)
            comentariosRepository.deletaComentarios(id, "post")
            call.respond(HttpStatusCode.Created)
        }

        // ---------- parte dos projetos ----------

        // adiciona um novo projeto
        post("/projetos"){
            val request = call.receive<ProjetoRequest>()
            val projeto = projetosRepository.criaProjeto(request.toProjeto())
            if(projeto != null){
                fotosRepository.criaFoto(projeto.id, "projeto", request.caminhoFoto)
                ciclosRepository.criaCiclo(projeto.id)
                call.respond(projeto.toProjetoResponse(ciclosRepository.achaCiclo(projeto.id), fotosRepository.achaFoto(projeto.id, "projeto")))
            }
            // call.respond(HttpStatusCode.Created)
        }

        // obtém um projeto a partir de um id
        get("/projeto/{id}"){
            val id = call.parameters.getOrFail<String>("id").toInt()
            val response = projetosRepository.obtemProjeto(id)
            if(response != null)
                call.respond(response.toProjetoResponse(ciclosRepository.achaCiclo(id), fotosRepository.achaFoto(id, "projeto")))
        }

        // obtém todos os projetos
        get("/projetos"){
            val response = projetosRepository.obtemProjetos()
                .map{it.toProjetoResponse(ciclosRepository.achaCiclo(it.id), fotosRepository.achaFoto(it.id, "projeto"))}
            call.respond(response)
        }

        // altera o ciclo de um projeto
        put("/projeto/{id}/ciclo"){
            val id = call.parameters.getOrFail<String>("id").toInt()
            val request = call.receive<Ciclo>()
            ciclosRepository.alteraCiclo(id, request)
            call.respond(HttpStatusCode.Created)
        }

        // altera um atributo do projeto
        put("/projeto/{id}") {
            val id = call.parameters.getOrFail<String>("id").toInt()
            val atributo = call.parameters["atributo"]
            val novoValor = call.receive<String>()

            when(atributo){
                "titulo" -> projetosRepository.alteraTitulo(id, novoValor)
                "descricao" -> projetosRepository.alteraDescricao(id, novoValor)
                "repositorio" -> projetosRepository.alteraRepositorio(id, novoValor)
                "foto" -> fotosRepository.alteraFoto(id, "projeto", novoValor)
            }

            call.respond(HttpStatusCode.Created)
        }

        // deleta o projeto
        delete("/projeto/{id}"){
            val id = call.parameters.getOrFail<String>("id").toInt()
            projetosRepository.deletaProjeto(id)
            comentariosRepository.deletaComentarios(id, "projeto")
            fotosRepository.deletaFoto(id, "projeto")

            call.respond(HttpStatusCode.Created)
        }

        // ---------- parte dos comentários ----------

        // adiciona um novo comentário
        post("/comentarios"){
            val request = call.receive<ComentarioRequest>()
            val comentario = comentariosRepository.criaComentario(request.toComentario())
            call.respond(HttpStatusCode.Created)
        }

        // obtém todos os comentários de um post
        get("/post/{id}/comentarios"){
            val id = call.parameters.getOrFail<String>("id").toInt()
            val response = comentariosRepository.obtemComentariosPai(id, "post")
                .map{(listOf(it) + comentariosRepository.obtemComentariosFilho(id, it.id, "post"))
                    .map{it.toComentarioResponse(usuariosRepository.obtemUsuario(it.idUsuario)?.nome)}}
            call.respond(response)
        }

        // obtém todos os comentários de um projeto
        get("/projeto/{id}/comentarios"){
            val id = call.parameters.getOrFail<String>("id").toInt()
            val response = comentariosRepository.obtemComentariosPai(id, "projeto")
                .map{(listOf(it) + comentariosRepository.obtemComentariosFilho(id, it.id, "projeto"))
                    .map{it.toComentarioResponse(usuariosRepository.obtemUsuario(it.idUsuario)?.nome)}}
            call.respond(response)
        }

        // ---------- parte do login ----------

        @Serializable
        data class t(val token: String)

        @Serializable
        data class Login(val email: String, val password: String)

        post("/login"){
            val params = call.receive<Login>()
            val email = params.email
            val senha = params.password

            val retorno = usuariosRepository.login(email, senha)

            if(retorno == null){
                val response = Json.encodeToString(t("-1"))
                call.respondText(response)
            }

            else{
                val response = Json.encodeToString(t("${retorno.id}"))
                call.respondText(response)
            }
        }
    }
}