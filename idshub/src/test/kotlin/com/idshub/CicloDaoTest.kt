package com.idshub

import com.idshub.database.DatabaseFactory
import com.idshub.models.Ciclo
import com.idshub.models.Projeto
import com.idshub.repositories.CiclosRepository
import com.idshub.repositories.ProjetosRepository
import kotlinx.coroutines.runBlocking
import java.io.File
import kotlin.test.*

class CicloDaoTest{
    val databaseFile = File("./build/teste.mv.db")
    val projetosRepository = ProjetosRepository()
    val projeto = Projeto(titulo = "titulo", descricao = "descricao", repositorio = "repositorio", idGrupo = 1)
    val ciclosRepository = CiclosRepository()
    val ciclo = Ciclo(1, "1/1/1", "2/2/2")

    @BeforeTest
    fun inicio(){
        DatabaseFactory.init(jdbcURL = "jdbc:h2:file:./build/teste")
    }

    @AfterTest
    fun fim(){
        databaseFile.delete()
    }

    @Test
    fun testeCriaEAchaCiclo() = runBlocking{
        val projetoCriado = projetosRepository.criaProjeto(projeto)
        val idProjetoCriado = projetoCriado?.id ?: 1
        ciclosRepository.criaCiclo(idProjetoCriado)
        val cicloObtido = ciclosRepository.achaCiclo(idProjetoCriado)
        assertEquals(0, cicloObtido?.numero)
        assertEquals("", cicloObtido?.inicio)
        assertEquals("", cicloObtido?.fim)
    }

    @Test
    fun testeAlteraCiclo() = runBlocking{
        val projetoCriado = projetosRepository.criaProjeto(projeto)
        val idProjetoCriado = projetoCriado?.id ?: 1
        ciclosRepository.criaCiclo(idProjetoCriado)
        ciclosRepository.alteraCiclo(idProjetoCriado, ciclo)
        val cicloObtido = ciclosRepository.achaCiclo(idProjetoCriado)
        assertEquals(ciclo.numero, cicloObtido?.numero)
        assertEquals(ciclo.inicio, cicloObtido?.inicio)
        assertEquals(ciclo.fim, cicloObtido?.fim)
    }
}