package com.idshub

import com.idshub.database.DatabaseFactory
import com.idshub.models.Usuario
import com.idshub.repositories.UsuariosRepository
import kotlinx.coroutines.runBlocking
import kotlin.test.*
import java.io.File

class UsuarioTest{
    val databaseFile = File("./build/teste.mv.db")
    val usuariosRepository = UsuariosRepository()
    val usuario = Usuario(nome = "vinicius", senha = "senha", email = "vinicius123@gmail.com")

    @BeforeTest
    fun inicio(){
        DatabaseFactory.init(jdbcURL = "jdbc:h2:file:./build/teste")
    }

    @AfterTest
    fun fim(){
        databaseFile.delete()
    }

    @Test
    fun testeCriaUsuario() = runBlocking{
        val usuarioCriado = usuariosRepository.criaUsuario(usuario)
        assertEquals("vinicius", usuarioCriado?.nome)
        assertEquals("vinicius123@gmail.com", usuarioCriado?.email)
        assertEquals("senha", usuarioCriado?.senha)
    }

    @Test
    fun testeEmailDisponivelLivre() = runBlocking{
        val valor = usuariosRepository.emailDisponivel("vinicius123@gmail.com")
        assertEquals(true, valor)
    }

    @Test
    fun testeEmailDisponivelOcupado() = runBlocking{
        val usuarioCriado = usuariosRepository.criaUsuario(usuario)
        val valor = usuariosRepository.emailDisponivel("vinicius123@gmail.com")
        assertEquals(false, valor)
    }

    @Test
    fun testeLoginCorreto() = runBlocking{
        val usuarioCriado = usuariosRepository.criaUsuario(usuario)
        val usuarioObtido = usuariosRepository.login(usuario.email, usuario.senha)
        assertEquals(usuarioCriado?.nome, usuarioObtido?.nome)
        assertEquals(usuarioCriado?.email, usuarioObtido?.email)
        assertEquals(usuarioCriado?.senha, usuarioObtido?.senha)
    }

    @Test
    fun testeLoginIncorreto() = runBlocking{
        val usuarioCriado = usuariosRepository.criaUsuario(usuario)
        val usuarioObtido = usuariosRepository.login(usuario.email, "outra senha")
        assertNotEquals(usuarioCriado?.nome, usuarioObtido?.nome)
        assertNotEquals(usuarioCriado?.email, usuarioObtido?.email)
        assertNotEquals(usuarioCriado?.senha, usuarioObtido?.senha)
    }

    @Test
    fun testeObterUsuario() = runBlocking{
        val usuarioCriado = usuariosRepository.criaUsuario(usuario)
        val idUsuarioCriado = usuarioCriado?.id ?: 1
        val usuarioObtido = usuariosRepository.obtemUsuario(idUsuarioCriado)
        assertEquals(usuarioCriado?.nome, usuarioObtido?.nome)
        assertEquals(usuarioCriado?.email, usuarioObtido?.email)
        assertEquals(usuarioCriado?.senha, usuarioObtido?.senha)
    }

    @Test
    fun testeAlterarNome() = runBlocking{
        val usuarioCriado = usuariosRepository.criaUsuario(usuario)
        val idUsuarioCriado = usuarioCriado?.id ?: 1
        usuariosRepository.alteraNome(idUsuarioCriado, "felipe")
        val usuarioObtido = usuariosRepository.obtemUsuario(idUsuarioCriado)
        assertEquals("felipe", usuarioObtido?.nome)
    }

    @Test
    fun testeAlterarEmail() = runBlocking{
        val usuarioCriado = usuariosRepository.criaUsuario(usuario)
        val idUsuarioCriado = usuarioCriado?.id ?: 1
        usuariosRepository.alteraEmail(idUsuarioCriado, "felipe123@gmail.com")
        val usuarioObtido = usuariosRepository.obtemUsuario(idUsuarioCriado)
        assertEquals("felipe123@gmail.com", usuarioObtido?.email)
    }

    @Test
    fun testeAlterarSenha() = runBlocking{
        val usuarioCriado = usuariosRepository.criaUsuario(usuario)
        val idUsuarioCriado = usuarioCriado?.id ?: 1
        usuariosRepository.alteraSenha(idUsuarioCriado, "senha2")
        val usuarioObtido = usuariosRepository.obtemUsuario(idUsuarioCriado)
        assertEquals("senha2", usuarioObtido?.senha)
    }
}
