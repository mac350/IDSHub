package com.idshub

import com.idshub.database.DatabaseFactory
import com.idshub.models.Post
import com.idshub.repositories.PostsRepository
import kotlinx.coroutines.runBlocking
import java.io.File
import kotlin.test.*

class PostDao{
    val databaseFile = File("./build/teste.mv.db")
    val postsRepository = PostsRepository()
    val post1 = Post(titulo = "titulo1", descricao = "descricao1", texto = "texto1", idGrupo = 1)
    val post2 = Post(titulo = "titulo2", descricao = "descricao2", texto = "texto2", idGrupo = 1)
    val post3 = Post(titulo = "titulo3", descricao = "descricao3", texto = "texto3", idGrupo = 2)

    @BeforeTest
    fun inicio(){
        DatabaseFactory.init(jdbcURL = "jdbc:h2:file:./build/teste")
    }

    @AfterTest
    fun fim(){
        databaseFile.delete()
    }

    @Test
    fun testeCriaPost() = runBlocking{
        val postCriado = postsRepository.criaPost(post1)
        assertEquals(post1.titulo, postCriado?.titulo)
        assertEquals(post1.descricao, postCriado?.descricao)
        assertEquals(post1.texto, postCriado?.texto)
        assertEquals(post1.idGrupo, postCriado?.idGrupo)
        assertEquals(post1.dataDeCriacao, postCriado?.dataDeCriacao)
        assertEquals(post1.dataDeAlteracao, postCriado?.dataDeAlteracao)
    }

    @Test
    fun testeObterPost() = runBlocking{
        val postCriado = postsRepository.criaPost(post1)
        val idPostCriado = postCriado?.id ?: 1
        val postObtido = postsRepository.obtemPost(idPostCriado)
        assertEquals(postCriado?.titulo, postObtido?.titulo)
        assertEquals(postCriado?.descricao, postObtido?.descricao)
        assertEquals(postCriado?.texto, postObtido?.texto)
        assertEquals(postCriado?.idGrupo, postObtido?.idGrupo)
        assertEquals(postCriado?.dataDeCriacao, postObtido?.dataDeCriacao)
        assertEquals(postCriado?.dataDeAlteracao, postObtido?.dataDeAlteracao)
    }

    @Test
    fun testeObterTodosPosts() = runBlocking{
        val postCriado1 = postsRepository.criaPost(post1)
        val postCriado2 = postsRepository.criaPost(post2)
        val postsObtidos = postsRepository.obtemPosts()
        assertEquals(postCriado1?.titulo, postsObtidos[0].titulo)
        assertEquals(postCriado1?.descricao, postsObtidos[0].descricao)
        assertEquals(postCriado1?.texto, postsObtidos[0].texto)
        assertEquals(postCriado1?.idGrupo, postsObtidos[0].idGrupo)
        assertEquals(postCriado1?.dataDeCriacao, postsObtidos[0].dataDeCriacao)
        assertEquals(postCriado1?.dataDeAlteracao, postsObtidos[0].dataDeAlteracao)
        assertEquals(postCriado2?.titulo, postsObtidos[1].titulo)
        assertEquals(postCriado2?.descricao, postsObtidos[1].descricao)
        assertEquals(postCriado2?.texto, postsObtidos[1].texto)
        assertEquals(postCriado2?.idGrupo, postsObtidos[1].idGrupo)
        assertEquals(postCriado2?.dataDeCriacao, postsObtidos[1].dataDeCriacao)
        assertEquals(postCriado2?.dataDeAlteracao, postsObtidos[1].dataDeAlteracao)
    }

    @Test
    fun testeObterPostsGrupo() = runBlocking{
        val postCriado1 = postsRepository.criaPost(post1)
        val postCriado2 = postsRepository.criaPost(post2)
        val postCriado3 = postsRepository.criaPost(post3)
        val postsObtidos = postsRepository.postsDoGrupo(1)
        assertEquals(2, postsObtidos.size)
        assertEquals(postCriado1?.titulo, postsObtidos[0].titulo)
        assertEquals(postCriado1?.descricao, postsObtidos[0].descricao)
        assertEquals(postCriado1?.texto, postsObtidos[0].texto)
        assertEquals(postCriado1?.idGrupo, postsObtidos[0].idGrupo)
        assertEquals(postCriado1?.dataDeCriacao, postsObtidos[0].dataDeCriacao)
        assertEquals(postCriado1?.dataDeAlteracao, postsObtidos[0].dataDeAlteracao)
        assertEquals(postCriado2?.titulo, postsObtidos[1].titulo)
        assertEquals(postCriado2?.descricao, postsObtidos[1].descricao)
        assertEquals(postCriado2?.texto, postsObtidos[1].texto)
        assertEquals(postCriado2?.idGrupo, postsObtidos[1].idGrupo)
        assertEquals(postCriado2?.dataDeCriacao, postsObtidos[1].dataDeCriacao)
        assertEquals(postCriado2?.dataDeAlteracao, postsObtidos[1].dataDeAlteracao)
    }

    @Test
    fun testeAlterarTitulo() = runBlocking{
        val postCriado = postsRepository.criaPost(post1)
        val idPostCriado = postCriado?.id ?: 1
        postsRepository.alteraTitulo(idPostCriado, "titulo2")
        val postObtido = postsRepository.obtemPost(idPostCriado)
        assertEquals("titulo2", postObtido?.titulo)
    }

    @Test
    fun testeAlterarDescricao() = runBlocking{
        val postCriado = postsRepository.criaPost(post1)
        val idPostCriado = postCriado?.id ?: 1
        postsRepository.alteraDescricao(idPostCriado, "descricao2")
        val postObtido = postsRepository.obtemPost(idPostCriado)
        assertEquals("descricao2", postObtido?.descricao)
    }

    @Test
    fun testeAlterarTexto() = runBlocking{
        val postCriado = postsRepository.criaPost(post1)
        val idPostCriado = postCriado?.id ?: 1
        postsRepository.alteraTexto(idPostCriado, "texto2")
        val postObtido = postsRepository.obtemPost(idPostCriado)
        assertEquals("texto2", postObtido?.texto)
    }

    @Test
    fun testeDeletarPost() = runBlocking{
        val postCriado = postsRepository.criaPost(post1)
        val idPostCriado = postCriado?.id ?: 1
        postsRepository.deletaPost(idPostCriado)
        val postObtido = postsRepository.obtemPost(idPostCriado)
        assertEquals(null, postObtido)
    }
}
