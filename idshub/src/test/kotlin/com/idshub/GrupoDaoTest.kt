package com.idshub

import com.idshub.database.DatabaseFactory
import com.idshub.models.Grupo
import com.idshub.repositories.GruposRepository
import kotlinx.coroutines.runBlocking
import kotlin.test.*
import java.io.File

class GrupoDaoTest{
    val databaseFile = File("./build/teste.mv.db")
    val gruposRepository = GruposRepository()
    val grupo1 = Grupo(nome = "grupo1")
    val grupo2 = Grupo(nome = "grupo2")

    @BeforeTest
    fun inicio(){
        DatabaseFactory.init(jdbcURL = "jdbc:h2:file:./build/teste")
    }

    @AfterTest
    fun fim(){
        databaseFile.delete()
    }

    @Test
    fun testeCriaGrupo() = runBlocking{
        val grupoCriado = gruposRepository.criaGrupo(grupo1)
        assertEquals("grupo1", grupoCriado?.nome)
        assertEquals(1, grupoCriado?.qtdMembros)
    }

    @Test
    fun testeObterGrupo() = runBlocking{
        val grupoCriado = gruposRepository.criaGrupo(grupo1)
        val idGrupoCriado = grupoCriado?.id ?: 1
        val grupoObtido = gruposRepository.obtemGrupo(idGrupoCriado)
        assertEquals(grupoCriado?.nome, grupoObtido?.nome)
    }

    @Test
    fun testeObterGrupos() = runBlocking{
        val grupoCriado1 = gruposRepository.criaGrupo(grupo1)
        val grupoCriado2 = gruposRepository.criaGrupo(grupo2)
        val gruposObtidos = gruposRepository.obtemGrupos()
        assertEquals(grupoCriado1?.nome, gruposObtidos[0].nome)
        assertEquals(grupoCriado2?.nome, gruposObtidos[1].nome)
    }

    @Test
    fun testeAlterarNome() = runBlocking{
        val grupoCriado = gruposRepository.criaGrupo(grupo1)
        val idGrupoCriado = grupoCriado?.id ?: 1
        gruposRepository.alteraNome(idGrupoCriado, "grupo2")
        val grupoObtido = gruposRepository.obtemGrupo(idGrupoCriado)
        assertEquals("grupo2", grupoObtido?.nome)
    }

    @Test
    fun testeSomaQtdMembros() = runBlocking{
        val grupoCriado = gruposRepository.criaGrupo(grupo1)
        val idGrupoCriado = grupoCriado?.id ?: 1
        gruposRepository.somaQtdMembros(idGrupoCriado)
        val grupoObtido = gruposRepository.obtemGrupo(idGrupoCriado)
        assertEquals(2, grupoObtido?.qtdMembros)
    }

    @Test
    fun testeSubtraiQtdMembros() = runBlocking{
        val grupoCriado = gruposRepository.criaGrupo(grupo1)
        val idGrupoCriado = grupoCriado?.id ?: 1
        gruposRepository.subtraiQtdMembros(idGrupoCriado)
        val grupoObtido = gruposRepository.obtemGrupo(idGrupoCriado)
        assertEquals(0, grupoObtido?.qtdMembros)
    }
}