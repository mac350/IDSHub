package com.idshub

import com.idshub.database.DatabaseFactory
import com.idshub.repositories.SolicitacoesRepository
import kotlinx.coroutines.runBlocking
import kotlin.test.*
import java.io.File

class SolicitacaoDao{
    val databaseFile = File("./build/teste.mv.db")
    val solicitacoesRepository = SolicitacoesRepository()

    @BeforeTest
    fun inicio(){
        DatabaseFactory.init(jdbcURL = "jdbc:h2:file:./build/teste")
    }

    @AfterTest
    fun fim(){
        databaseFile.delete()
    }

    @Test
    fun testCriaEExisteRelacao() = runBlocking{
        solicitacoesRepository.criaRelacao(1, 1)
        assertEquals(true, solicitacoesRepository.existeRelacao(1, 1))
        assertEquals(false, solicitacoesRepository.existeRelacao(0, 1))
        assertEquals(false, solicitacoesRepository.existeRelacao(1, 0))
    }

    @Test
    fun testAchaIdUsuarios() = runBlocking{
        solicitacoesRepository.criaRelacao(1, 1)
        solicitacoesRepository.criaRelacao(2, 1)
        val idUsuarios = solicitacoesRepository.achaIdUsuarios(1)
        assertEquals(1, idUsuarios[0])
        assertEquals(2, idUsuarios[1])
    }

    @Test
    fun testRemoveRelacao() = runBlocking{
        solicitacoesRepository.criaRelacao(1, 1)
        solicitacoesRepository.removeRelacao(1, 1)
        assertEquals(false, solicitacoesRepository.existeRelacao(1, 1))
    }
}