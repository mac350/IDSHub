package com.idshub

import com.idshub.database.DatabaseFactory
import com.idshub.models.Foto
import com.idshub.repositories.FotosRepository
import kotlinx.coroutines.runBlocking
import java.io.File
import kotlin.test.*

class FotoDaoTest{
    val databaseFile = File("./build/teste.mv.db")
    val fotosRepository = FotosRepository()

    @BeforeTest
    fun inicio(){
        DatabaseFactory.init(jdbcURL = "jdbc:h2:file:./build/teste")
    }

    @AfterTest
    fun fim(){
        databaseFile.delete()
    }

    @Test
    fun testeCriaEAchaFoto() = runBlocking{
        fotosRepository.criaFoto(1, "projeto", "caminho")
        val fotoObtida = fotosRepository.achaFoto(1, "projeto")
        assertEquals("caminho", fotoObtida)
    }

    @Test
    fun testeAlteraFoto() = runBlocking{
        fotosRepository.criaFoto(1, "projeto", "caminho")
        fotosRepository.alteraFoto(1, "projeto", "caminho2")
        val fotoObtida = fotosRepository.achaFoto(1, "projeto")
        assertEquals("caminho2", fotoObtida)
    }
}