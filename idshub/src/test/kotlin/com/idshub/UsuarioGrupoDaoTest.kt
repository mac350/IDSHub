package com.idshub

import com.idshub.database.DatabaseFactory
import com.idshub.repositories.UsuariosGruposRepository
import kotlinx.coroutines.runBlocking
import kotlin.test.*
import java.io.File


class UsuarioGrupoDao{
    val databaseFile = File("./build/teste.mv.db")
    val usuariosGruposRepository = UsuariosGruposRepository()

    @BeforeTest
    fun inicio(){
        DatabaseFactory.init(jdbcURL = "jdbc:h2:file:./build/teste")
    }

    @AfterTest
    fun fim(){
        databaseFile.delete()
    }

    @Test
    fun testeCriaEExisteRelacao() = runBlocking{
        usuariosGruposRepository.criaRelacao(1, 1)
        assertEquals(true, usuariosGruposRepository.existeRelacao(1, 1))
        assertEquals(false, usuariosGruposRepository.existeRelacao(0, 1))
        assertEquals(false, usuariosGruposRepository.existeRelacao(1, 0))
    }

    @Test
    fun testeAchaIdGrupos() = runBlocking{
        usuariosGruposRepository.criaRelacao(1, 1)
        usuariosGruposRepository.criaRelacao(1, 2)
        val idGrupos = usuariosGruposRepository.achaIdGrupos(1)
        assertEquals(1, idGrupos[0])
        assertEquals(2, idGrupos[1])
    }

    @Test
    fun testeAchaIdUsuarios() = runBlocking{
        usuariosGruposRepository.criaRelacao(1, 1)
        usuariosGruposRepository.criaRelacao(2, 1)
        val idUsuarios = usuariosGruposRepository.achaIdUsuarios(1)
        assertEquals(1, idUsuarios[0])
        assertEquals(2, idUsuarios[1])
    }

    @Test
    fun testeRemoveRelacao() = runBlocking{
        usuariosGruposRepository.criaRelacao(1, 1)
        usuariosGruposRepository.removeRelacao(1, 1)
        assertEquals(false, usuariosGruposRepository.existeRelacao(1, 1))
    }
}