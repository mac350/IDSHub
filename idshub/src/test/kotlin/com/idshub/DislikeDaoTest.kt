package com.idshub

import com.idshub.database.DatabaseFactory
import com.idshub.models.Comentario
import com.idshub.repositories.ComentariosRepository
import com.idshub.repositories.DislikesRepository
import java.io.File
import kotlinx.coroutines.runBlocking
import kotlin.test.*

class DislikeDaoTest{
    val databaseFile = File("./build/teste.mv.db")
    val comentariosRepository = ComentariosRepository()
    val dislikesRepository = DislikesRepository()
    val comentario = Comentario(idUsuario = 1, idConteudo = 1, tipoConteudo = "projeto", texto = "texto")

    @BeforeTest
    fun inicio(){
        DatabaseFactory.init(jdbcURL = "jdbc:h2:file:./build/teste")
    }

    @AfterTest
    fun fim(){
        databaseFile.delete()
    }

    @Test
    fun testeDarDislikeEDeuDislike() = runBlocking{
        val comentarioCriado = comentariosRepository.criaComentario(comentario)
        val idComentarioCriado = comentarioCriado?.id ?: 1
        dislikesRepository.darDislike(1, idComentarioCriado)
        assertEquals(true, dislikesRepository.deuDislike(1, idComentarioCriado))
    }

    @Test
    fun testeRetirarDislikeEDeuDislike() = runBlocking{
        val comentarioCriado = comentariosRepository.criaComentario(comentario)
        val idComentarioCriado = comentarioCriado?.id ?: 1
        dislikesRepository.darDislike(1, idComentarioCriado)
        dislikesRepository.retirarDislike(1, idComentarioCriado)
        assertEquals(false, dislikesRepository.deuDislike(1, idComentarioCriado))
    }
}