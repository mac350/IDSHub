package com.idshub

import com.idshub.database.DatabaseFactory
import com.idshub.models.Comentario
import com.idshub.repositories.ComentariosRepository
import kotlinx.coroutines.runBlocking
import java.io.File
import kotlin.test.*

class ComentarioDaoTest{
    val databaseFile = File("./build/teste.mv.db")
    val comentariosRepository = ComentariosRepository()
    val comentario1 = Comentario(idUsuario = 1, idConteudo = 1, tipoConteudo = "projeto", texto = "texto1")
    val comentario2 = Comentario(idUsuario = 1, idConteudo = 1, tipoConteudo = "projeto", texto = "texto2")

    @BeforeTest
    fun inicio(){
        DatabaseFactory.init(jdbcURL = "jdbc:h2:file:./build/teste")
    }

    @AfterTest
    fun fim(){
        databaseFile.delete()
    }

    @Test
    fun testeCriaComentario() = runBlocking{
        val comentarioCriado = comentariosRepository.criaComentario(comentario1)
        assertEquals(comentario1.idUsuario, comentarioCriado?.idUsuario)
        assertEquals(comentario1.idConteudo, comentarioCriado?.idConteudo)
        assertEquals(comentario1.tipoConteudo, comentarioCriado?.tipoConteudo)
        assertEquals(comentario1.texto, comentarioCriado?.texto)
        assertEquals(comentario1.dataDeRealizacao, comentarioCriado?.dataDeRealizacao)
        assertEquals(comentario1.qtdLikes, comentarioCriado?.qtdLikes)
        assertEquals(comentario1.qtdDislikes, comentarioCriado?.qtdDislikes)
    }

    @Test
    fun testeObtemComentariosPai() = runBlocking{
        val comentarioCriado1 = comentariosRepository.criaComentario(comentario1)
        val comentarioCriado2 = comentariosRepository.criaComentario(comentario2)
        val comentariosObtidos = comentariosRepository.obtemComentariosPai(1, "projeto")
        assertEquals(2, comentariosObtidos.size)
        assertEquals("texto1", comentariosObtidos[0].texto)
        assertEquals("texto2", comentariosObtidos[1].texto)
    }

    @Test
    fun testeObtemComentariosFilhos() = runBlocking{
        val comentarioCriado1 = comentariosRepository.criaComentario(comentario1)
        val comentarioCriado2 = comentariosRepository.criaComentario(comentario2)
        val idComentarioCriado1 = comentarioCriado1?.id ?: 1
        val idComentarioCriado2 = comentarioCriado2?.id ?: 2
        val comentario3 = Comentario(idUsuario = 1, idConteudo = 1, tipoConteudo = "projeto", texto = "texto3", idComentarioPai = idComentarioCriado1)
        val comentario4 = Comentario(idUsuario = 1, idConteudo = 1, tipoConteudo = "projeto", texto = "texto4", idComentarioPai = idComentarioCriado1)
        val comentario5 = Comentario(idUsuario = 1, idConteudo = 1, tipoConteudo = "projeto", texto = "texto5", idComentarioPai = idComentarioCriado2)
        val comentario6 = Comentario(idUsuario = 1, idConteudo = 1, tipoConteudo = "projeto", texto = "texto6", idComentarioPai = idComentarioCriado2)
        val comentarioCriado3 = comentariosRepository.criaComentario(comentario3)
        val comentarioCriado4 = comentariosRepository.criaComentario(comentario4)
        val comentarioCriado5 = comentariosRepository.criaComentario(comentario5)
        val comentarioCriado6 = comentariosRepository.criaComentario(comentario6)
        val comentariosFilhos1 = comentariosRepository.obtemComentariosFilho(1, idComentarioCriado1, "projeto")
        val comentariosFilhos2 = comentariosRepository.obtemComentariosFilho(1, idComentarioCriado2, "projeto")
        assertEquals(2, comentariosFilhos1.size)
        assertEquals("texto3", comentariosFilhos1[0].texto)
        assertEquals("texto4", comentariosFilhos1[1].texto)
        assertEquals(2, comentariosFilhos2.size)
        assertEquals("texto5", comentariosFilhos2[0].texto)
        assertEquals("texto6", comentariosFilhos2[1].texto)
    }

    @Test
    fun testeDeletarComentario() = runBlocking{
        val comentarioCriado1 = comentariosRepository.criaComentario(comentario1)
        val idComentarioCriado1 = comentarioCriado1?.id ?: 1
        val comentario3 = Comentario(idUsuario = 1, idConteudo = 1, tipoConteudo = "projeto", texto = "texto3", idComentarioPai = idComentarioCriado1)
        val comentarioCriado3 = comentariosRepository.criaComentario(comentario3)
        comentariosRepository.deletaComentarios(1, "projeto")
        val comentariosPai = comentariosRepository.obtemComentariosPai(1, "projeto")
        val comentariosFilhos1 = comentariosRepository.obtemComentariosFilho(1, idComentarioCriado1, "projeto")
        assertEquals(0, comentariosPai.size)
        assertEquals(0, comentariosFilhos1.size)
    }

    @Test
    fun testeSomaLike() = runBlocking{
        val comentarioCriado1 = comentariosRepository.criaComentario(comentario1)
        val idComentarioCriado1 = comentarioCriado1?.id ?: 1
        comentariosRepository.somaLike(idComentarioCriado1)
        val comentariosPai = comentariosRepository.obtemComentariosPai(1, "projeto")
        assertEquals(1, comentariosPai[0].qtdLikes)
    }

    @Test
    fun testeSubtraiLike() = runBlocking{
        val comentarioCriado1 = comentariosRepository.criaComentario(comentario1)
        val idComentarioCriado1 = comentarioCriado1?.id ?: 1
        comentariosRepository.subtraiLike(idComentarioCriado1)
        val comentariosPai = comentariosRepository.obtemComentariosPai(1, "projeto")
        assertEquals(-1, comentariosPai[0].qtdLikes)
    }

    @Test
    fun testeSomaDislike() = runBlocking{
        val comentarioCriado1 = comentariosRepository.criaComentario(comentario1)
        val idComentarioCriado1 = comentarioCriado1?.id ?: 1
        comentariosRepository.somaDislike(idComentarioCriado1)
        val comentariosPai = comentariosRepository.obtemComentariosPai(1, "projeto")
        assertEquals(1, comentariosPai[0].qtdDislikes)
    }

    @Test
    fun testeSubtraiDislike() = runBlocking{
        val comentarioCriado1 = comentariosRepository.criaComentario(comentario1)
        val idComentarioCriado1 = comentarioCriado1?.id ?: 1
        comentariosRepository.subtraiDislike(idComentarioCriado1)
        val comentariosPai = comentariosRepository.obtemComentariosPai(1, "projeto")
        assertEquals(-1, comentariosPai[0].qtdDislikes)
    }
}