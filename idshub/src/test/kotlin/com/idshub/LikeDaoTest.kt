package com.idshub

import com.idshub.database.DatabaseFactory
import com.idshub.models.Comentario
import com.idshub.repositories.ComentariosRepository
import com.idshub.repositories.LikesRepository
import java.io.File
import kotlinx.coroutines.runBlocking
import kotlin.test.*

class LikeDaoTest{
    val databaseFile = File("./build/teste.mv.db")
    val comentariosRepository = ComentariosRepository()
    val likesRepository = LikesRepository()
    val comentario = Comentario(idUsuario = 1, idConteudo = 1, tipoConteudo = "projeto", texto = "texto")

    @BeforeTest
    fun inicio(){
        DatabaseFactory.init(jdbcURL = "jdbc:h2:file:./build/teste")
    }

    @AfterTest
    fun fim(){
        databaseFile.delete()
    }

    @Test
    fun testeDarLikeEDeuLike() = runBlocking{
        val comentarioCriado = comentariosRepository.criaComentario(comentario)
        val idComentarioCriado = comentarioCriado?.id ?: 1
        likesRepository.darLike(1, idComentarioCriado)
        assertEquals(true, likesRepository.deuLike(1, idComentarioCriado))
    }

    fun testeRetirarLikeEDeuLike() = runBlocking{
        val comentarioCriado = comentariosRepository.criaComentario(comentario)
        val idComentarioCriado = comentarioCriado?.id ?: 1
        likesRepository.darLike(1, idComentarioCriado)
        likesRepository.retirarLike(1, idComentarioCriado)
        assertEquals(false, likesRepository.deuLike(1, idComentarioCriado))
    }
}