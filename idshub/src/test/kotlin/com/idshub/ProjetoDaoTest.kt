package com.idshub

import com.idshub.database.DatabaseFactory
import com.idshub.models.Projeto
import com.idshub.repositories.ProjetosRepository
import kotlinx.coroutines.runBlocking
import java.io.File
import kotlin.test.*

class ProjetoDaoTest{
    val databaseFile = File("./build/teste.mv.db")
    val projetosRepository = ProjetosRepository()
    val projeto1 = Projeto(titulo = "titulo1", descricao = "descricao1", repositorio = "repositorio1", idGrupo = 1)
    val projeto2 = Projeto(titulo = "titulo2", descricao = "descricao2", repositorio = "repositorio2", idGrupo = 1)
    val projeto3 = Projeto(titulo = "titulo3", descricao = "descricao3", repositorio = "repositorio3", idGrupo = 2)

    @BeforeTest
    fun inicio(){
        DatabaseFactory.init(jdbcURL = "jdbc:h2:file:./build/teste")
    }

    @AfterTest
    fun fim(){
        databaseFile.delete()
    }

    @Test
    fun testeCriaProjeto() = runBlocking{
        val projetoCriado = projetosRepository.criaProjeto(projeto1)
        assertEquals(projeto1.titulo, projetoCriado?.titulo)
        assertEquals(projeto1.descricao, projetoCriado?.descricao)
        assertEquals(projeto1.repositorio, projetoCriado?.repositorio)
        assertEquals(projeto1.idGrupo, projetoCriado?.idGrupo)
        assertEquals(projeto1.dataDeCriacao, projetoCriado?.dataDeCriacao)
        assertEquals(projeto1.dataDeAlteracao, projetoCriado?.dataDeAlteracao)
    }

    @Test
    fun testeObterProjeto() = runBlocking{
        val projetoCriado = projetosRepository.criaProjeto(projeto1)
        val idProjetoCriado = projetoCriado?.id ?: 1
        val projetoObtido = projetosRepository.obtemProjeto(idProjetoCriado)
        assertEquals(projetoCriado?.titulo, projetoObtido?.titulo)
        assertEquals(projetoCriado?.descricao, projetoObtido?.descricao)
        assertEquals(projetoCriado?.repositorio, projetoObtido?.repositorio)
        assertEquals(projetoCriado?.idGrupo, projetoObtido?.idGrupo)
        assertEquals(projetoCriado?.dataDeCriacao, projetoObtido?.dataDeCriacao)
        assertEquals(projetoCriado?.dataDeAlteracao, projetoObtido?.dataDeAlteracao)
    }

    @Test
    fun testeObterTodosProjetos() = runBlocking{
        val projetoCriado1 = projetosRepository.criaProjeto(projeto1)
        val projetoCriado2 = projetosRepository.criaProjeto(projeto2)
        val projetosObtidos = projetosRepository.obtemProjetos()
        assertEquals(projetoCriado1?.titulo, projetosObtidos[0].titulo)
        assertEquals(projetoCriado1?.descricao, projetosObtidos[0].descricao)
        assertEquals(projetoCriado1?.descricao, projetosObtidos[0].descricao)
        assertEquals(projetoCriado1?.idGrupo, projetosObtidos[0].idGrupo)
        assertEquals(projetoCriado1?.dataDeCriacao, projetosObtidos[0].dataDeCriacao)
        assertEquals(projetoCriado1?.dataDeAlteracao, projetosObtidos[0].dataDeAlteracao)
        assertEquals(projetoCriado2?.titulo, projetosObtidos[1].titulo)
        assertEquals(projetoCriado2?.descricao, projetosObtidos[1].descricao)
        assertEquals(projetoCriado2?.descricao, projetosObtidos[1].descricao)
        assertEquals(projetoCriado2?.idGrupo, projetosObtidos[1].idGrupo)
        assertEquals(projetoCriado2?.dataDeCriacao, projetosObtidos[1].dataDeCriacao)
        assertEquals(projetoCriado2?.dataDeAlteracao, projetosObtidos[1].dataDeAlteracao)
    }

    @Test
    fun testeObterProjetosGrupo() = runBlocking{
        val projetoCriado1 = projetosRepository.criaProjeto(projeto1)
        val projetoCriado2 = projetosRepository.criaProjeto(projeto2)
        val projetoCriado3 = projetosRepository.criaProjeto(projeto3)
        val projetosObtidos = projetosRepository.projetosDoGrupo(1)
        assertEquals(2, projetosObtidos.size)
        assertEquals(projetoCriado1?.titulo, projetosObtidos[0].titulo)
        assertEquals(projetoCriado1?.descricao, projetosObtidos[0].descricao)
        assertEquals(projetoCriado1?.descricao, projetosObtidos[0].descricao)
        assertEquals(projetoCriado1?.idGrupo, projetosObtidos[0].idGrupo)
        assertEquals(projetoCriado1?.dataDeCriacao, projetosObtidos[0].dataDeCriacao)
        assertEquals(projetoCriado1?.dataDeAlteracao, projetosObtidos[0].dataDeAlteracao)
        assertEquals(projetoCriado2?.titulo, projetosObtidos[1].titulo)
        assertEquals(projetoCriado2?.descricao, projetosObtidos[1].descricao)
        assertEquals(projetoCriado2?.descricao, projetosObtidos[1].descricao)
        assertEquals(projetoCriado2?.idGrupo, projetosObtidos[1].idGrupo)
        assertEquals(projetoCriado2?.dataDeCriacao, projetosObtidos[1].dataDeCriacao)
        assertEquals(projetoCriado2?.dataDeAlteracao, projetosObtidos[1].dataDeAlteracao)
    }

    @Test
    fun testAlterarTitulo() = runBlocking{
        val projetoCriado = projetosRepository.criaProjeto(projeto1)
        val idProjetoCriado = projetoCriado?.id ?: 1
        projetosRepository.alteraTitulo(idProjetoCriado, "titulo2")
        val projetoObtido = projetosRepository.obtemProjeto(idProjetoCriado)
        assertEquals("titulo2", projetoObtido?.titulo)
    }

    @Test
    fun testeAlterarDescricao() = runBlocking{
        val projetoCriado = projetosRepository.criaProjeto(projeto1)
        val idProjetoCriado = projetoCriado?.id ?: 1
        projetosRepository.alteraDescricao(idProjetoCriado, "descricao2")
        val projetoObtido = projetosRepository.obtemProjeto(idProjetoCriado)
        assertEquals("descricao2", projetoObtido?.descricao)
    }

    @Test
    fun testeAlterarRepositorio() = runBlocking{
        val projetoCriado = projetosRepository.criaProjeto(projeto1)
        val idProjetoCriado = projetoCriado?.id ?: 1
        projetosRepository.alteraRepositorio(idProjetoCriado, "repositorio2")
        val projetoObtido = projetosRepository.obtemProjeto(idProjetoCriado)
        assertEquals("repositorio2", projetoObtido?.repositorio)
    }

    @Test
    fun testeDeletarProjeto() = runBlocking{
        val projetoCriado = projetosRepository.criaProjeto(projeto1)
        val idProjetoCriado = projetoCriado?.id ?: 1
        projetosRepository.deletaProjeto(idProjetoCriado)
        val projetoObtido = projetosRepository.obtemProjeto(idProjetoCriado)
        assertEquals(null, projetoObtido)
    }
}
