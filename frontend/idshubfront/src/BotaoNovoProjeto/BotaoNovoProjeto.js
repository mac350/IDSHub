import { useNavigate } from "react-router-dom";
import { useState } from "react";
import { useEffect } from "react";
import useToken from "../useToken";


function BotaoNovoProjeto() {
    const navigate = useNavigate();
    const [grupos, setGrupos] = useState([]);
    const { token, setToken } = useToken();

    const pegarGrupos = () => {
        
        if (!token){
            setGrupos([]);
            return;
        }
        fetch("http://localhost:8080/usuario/" + token + "/grupos",{
            method: "GET"
        })
        .then((res) => {
            return res.json();
        })
        .then((data) => {
            console.log(data);
            setGrupos(data);
        });
    };

    const enviarRequestParaProjeto = (event) => {
        event.preventDefault();

        var grupoId = document.getElementById("selectEl").value;

        if (grupoId == 0){
            alert("Escolha um grupo antes de criar um projeto");
            return;
        }
        const data = {
            titulo: "Projeto Novo",
            idGrupo: grupoId,
            descricao: "Clique nesse projeto e edite as informações",
            repositorio: "https://www.github.com",
            caminhoFoto: "fotoPadraoProjeto"
        };

        fetch("http://localhost:8080/projetos", {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        })
        .then(response => {
            if (response.ok) {
                console.log('Dados enviados com sucesso!');
                return response.json();
            } else {
                console.error('Erro ao enviar os dados.');
            }
        }).then((projeto) => {
            console.log(projeto);
            navigate("/editarprojeto?id="+projeto.id);
        })
        .catch(error => {
            console.error('Erro ao enviar os dados:', error);
        })
        };

    useEffect(() => {
        pegarGrupos();
    }, []);

    return (
        <div className="row justify-content-center">
            <div className="col-auto">
                <select id="selectEl" class="form-select" aria-label="Default select example">
                    <option value={0}>Escolha um grupo para criar um projeto</option>
                    {
                        grupos.map((grupo) => (
                            <option value={grupo.id}>{grupo.nome}</option>
                        ))
                    }
                </select>
            </div>
            <div className="col-auto">
                <button onClick={enviarRequestParaProjeto} className=" text-center mx-1 btn-primary btn">Criar Novo Projeto</button>
            </div>
        </div>
    );
}

export default BotaoNovoProjeto;