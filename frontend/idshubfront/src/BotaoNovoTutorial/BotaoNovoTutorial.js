import { useNavigate } from "react-router-dom";
import { useState } from "react";
import { useEffect } from "react";
import useToken from "../useToken";

function BotaoNovoTutorial() {
    const navigate = useNavigate();
    const [grupos, setGrupos] = useState([]);
    const { token, setToken } = useToken();

    const pegarGrupos = () => {

        if (!token) {
            setGrupos([]);
            return;
        }
        fetch("http://localhost:8080/usuario/" + token + "/grupos", {
            method: "GET"
        })
            .then((res) => {
                return res.json();
            })
            .then((data) => {
                console.log(data);
                setGrupos(data);
            });
    };

    const enviarRequestParaTutorial = (event) => {
        event.preventDefault();

        var grupoId = document.getElementById("selectEl").value;

        if (grupoId == 0) {
            alert("Escolha um grupo antes de criar um tutorial");
            return;
        }

        const data = {
            titulo: "Tutorial Novo",
            idGrupo: grupoId,
            texto: "Clique nesse tutorial e edite as informações",
            descricao: "Clique nesse tutorial e edite as informações"
        };

        fetch("http://localhost:8080/posts", {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        })
            .then(response => {
                if (response.ok) {
                    console.log('Dados enviados com sucesso!');
                    return response.json();
                } else {
                    console.error('Erro ao enviar os dados.');
                }
            }).then((tutorial) => {
                console.log(tutorial);
                navigate("/editartutorial?id=" + tutorial.id);
            })
            .catch(error => {
                console.error('Erro ao enviar os dados:', error);
            })
    };

    useEffect(() => {
        pegarGrupos();
    }, []);

    return (
        <div className="row justify-content-center">
            <div className="col-auto">
                <select id="selectEl" className="form-select" aria-label="Default select example">
                    <option value={0}>Escolha um grupo para criar um projeto</option>
                    {
                        grupos.map((grupo) => (
                            <option value={grupo.id}>{grupo.nome}</option>
                        ))
                    }
                </select>
            </div>
            <div className="col-auto">
                <button onClick={enviarRequestParaTutorial} className=" text-center mx-1 btn-primary btn">Criar Novo Tutorial</button>
            </div>
        </div>
    );
}

export default BotaoNovoTutorial;