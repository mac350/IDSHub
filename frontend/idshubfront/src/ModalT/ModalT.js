function ModalT(props) {

    const handleSubmit = (event) => {
        event.preventDefault();

        var novoTitulo = (document.getElementById(props.id+"input").value);

        fetch("http://localhost:8080/post/"+props.idProjeto+"?atributo="+props.atributo, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json'
            },
            body: novoTitulo
        })
        .then(response => {
            if (response.ok) {
                console.log('Dados enviados com sucesso!');
                window.location.reload();
            } else {
                console.error('Erro ao enviar os dados.');
            }
        })
        .catch(error => {
            console.error('Erro ao enviar os dados:', error);
        }).then(()=>{
        })
    };
        
    if (props.type!="textarea"){
        return (
            <div className="modal fade" id={props.id} tabIndex="-1">
                <div className="modal-dialog">
                    <div className="modal-content">
                    <div className="modal-header">
                        <h1 className="modal-title fs-5" id={props.labelId}>{props.titulo}</h1>
                        <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div className="modal-body">
                        <input id={props.id + "input"} placeholder="..." type="text-area" className="form-control"/>
                    </div>
                    <div className="modal-footer">
                        <button type="button" className="btn btn-secondary" data-bs-dismiss="modal">Fechar</button>
                        <button type="button" onClick={handleSubmit} className="btn btn-primary" data-bs-dismiss="modal">Salvar Mudanças</button>
                    </div>
                    </div>
                </div>
            </div>
        );
    }
    else{
        return (
            <div className="modal fade" id={props.id} tabIndex="-1">
                <div className="modal-dialog">
                    <div className="modal-content">
                    <div className="modal-header">
                        <h1 className="modal-title fs-5" id={props.labelId}>{props.titulo}</h1>
                        <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div className="modal-body">
                        <textarea id={props.id + "input"} type="text-area" placeholder="Digite aqui a nova descrição" className="form-control" rows="15">{props.texto}</textarea>
                    </div>
                    <div className="modal-footer">
                        <button type="button" className="btn btn-secondary" data-bs-dismiss="modal">Fechar</button>
                        <button type="button" onClick={handleSubmit} className="btn btn-primary" data-bs-dismiss="modal">Salvar Mudanças</button>
                    </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default ModalT;