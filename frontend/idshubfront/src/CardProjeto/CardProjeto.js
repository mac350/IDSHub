import './CardProjeto.css';
import { Link } from 'react-router-dom';
import timeFromGivenDate from '../auxiliares.js';


function CardProjeto(props) {
    if (props.edit) {
        return (
            <div className="card mb-3">
                <div className='row'>
                    <div className='col-md-4'>
                        <img src={props.imgsrc} className="img-fluid rounded-start img-cover"/>
                    </div>
                    <div className='col-md-8'>
                        <div className="card-body">
                            <h5 className="card-title">{props.nome}</h5>
                            <p className="card-text descr-projeto">{props.descr}</p>
                            <p className="card-text"><small className="text-body-secondary">Última atualização {timeFromGivenDate(props.time)} <br />Ciclo Atual: {props.ciclo}</small></p>
                            <Link to={'/Projeto?id=' + props.id}><button className="mx-1 btn-primary btn">Ver projeto</button></Link>
                            <a href={props.repositorio}><span className="mx-1 br">Ver repositório</span></a>
                            <Link className="mx-1 br" to={'/editarprojeto?id=' + props.id}>Editar</Link>
                        </div>
                    </div>

                </div>

            </div>
        );
    }

    return (
        <div className="card mb-3">
            <div className='row'>
                <div className='col-md-4'>
                    <img src={props.imgsrc} className="img-fluid rounded-start img-cover"></img>
                </div>
                <div className='col-md-8'>
                    <div className="card-body">
                        <h5 className="card-title">{props.nome}</h5>
                        <p className="card-text descr-projeto">{props.descr}</p>
                        <p className="card-text"><small className="text-body-secondary">Última atualização {timeFromGivenDate(props.time)} <br />Ciclo Atual: {props.ciclo}</small></p>
                        <Link to={'/Projeto?id=' + props.id}><button className="btn-primary btn">Ver projeto</button></Link>
                        <a href={props.repositorio}><span className="mx-1 br">Ver repositório</span></a>
                    </div>
                </div>

            </div>

        </div>
    );
}

export default CardProjeto;