import { useEffect } from "react";
import { useState } from "react";
import CardProjeto from "../CardProjeto/CardProjeto";
import useToken from "../useToken";
import CardTutorial from "../CardTutorial/CardTutorial";
import BotaoNovoTutorial from "../BotaoNovoTutorial/BotaoNovoTutorial";

function Projetos() {

    const [tutoriais, setTutoriais] = useState([]);
    const [ciclo, setCiclo] = useState([]);
    const { token, setToken } = useToken();

    useEffect(() => {
        fetch("http://localhost:8080/usuario/"+token+"/posts",{
            method: "GET"
        })
            .then((res) => {
                console.log(res);
                return res.json();
            })
            .then((data) => {
                setTutoriais(data);
            });
    }, []);

    return (
        <div className="row mt-5 justify-content-center">
            <div className="col-md-9 text-center mb-3">
                <BotaoNovoTutorial/>
            </div>

            <div className="col-md-9">

                {
                    tutoriais.map((tutorial) => (
                        <CardTutorial imgsrc={tutorial.foto?.caminho} key={tutorial.id} nome={tutorial.titulo} descr={tutorial.descricao} ciclo={tutorial.ciclo?.numero} time={tutorial.dataDeModificacao} id={tutorial.id} repositorio={tutorial.repositorio} edit={true}/>
                    ))
                }
            </div>
                
        </div>
    );
}

export default Projetos;
