import React, { useState } from 'react';

function Register() {

    const [nome, setNome] = useState('');
    const [email, setEmail] = useState('');
    const [senha, setSenha] = useState('');
    const [caminhoFoto, setFoto] = useState('fotoPadraoProjeto');

    const handleSubmit = (event) => {
        event.preventDefault();

        const data = {
            nome: nome,
            email: email,
            senha: senha,
            caminhoFoto: caminhoFoto
        };

        fetch("http://localhost:8080/criarUsuario", {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        })
            .then(response => {
                if (response.ok) {
                    console.log('Dados enviados com sucesso!');
                } else {
                    console.error('Erro ao enviar os dados.');
                }
            })
            .catch(error => {
                console.error('Erro ao enviar os dados:', error);
            });
        };

    return (
        <div className="row justify-content-center mt-5">
            <div className="col-md-6">
                <h1 className="titulo-fino">Criar Conta</h1>

        <form onSubmit={handleSubmit}>
            <div className="mb-3">            
                <label htmlFor="exampleFormControlInput1" className="form-label">Nome do usuário</label>
                <input 
                    type="text" className="form-control" id="nomeLogin" name="nomeLogin" placeholder="meu_nome123"
                    value={nome}
                    onChange={(event) => setNome(event.target.value)}
                />
            </div>
            <div className="mb-3">            
                <label htmlFor="exampleFormControlInput1" className="form-label">Endereço de Email</label>
                <input 
                    type="email"
                    className="form-control" id="emailLogin" name="emailLogin" placeholder="name@example.com"
                    value={email}
                    onChange={(event) => setEmail(event.target.value)}
                />
            </div>
            <div className="mb-3">            
                <label htmlFor="exampleFormControlInput1" className="form-label">Endereço para Foto (opcional)</label>
                <input
                    className="form-control" id="foto" name="foto" placeholder="www.google.com/minhafoto"
                    onChange={(event) => setFoto(event.target.value)}
                />
            </div>
            <div className="mb-3">            
                <label htmlFor="exampleFormControlTextarea1" className="form-label">Senha</label>
                <input
                    type="password" className="form-control" id="senhaConfLogin" name="senhaConfLogin"
                    value={senha}
                    onChange={(event) => setSenha(event.target.value)}
                />
            </div>
            <button type="submit" className="btn btn-primary">Criar Conta</button>
        </form>
            </div>
        </div>
    );
}

export default Register;

