import PropTypes from 'prop-types';
import { useState } from 'react';
import { useLocation, useNavigate } from 'react-router-dom';


// As funções de login e gerenciamento de sessões foram baseadas neste tutorial: https://www.digitalocean.com/community/tutorials/how-to-add-login-authentication-to-react-applications

async function loginUser(credentials) {
    return fetch('http://localhost:8080/login', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(credentials)
    })
     .then(data => data.json())
}

function Login({setToken}) {
    const navigate = useNavigate();
    const [email, setEmail] = useState();
    const [password, setPassword] = useState();

    // Ao enviar formulário Salvar o token
    const handleSubmit = async e => {
        e.preventDefault();
        const token = await loginUser({
          email,
          password
        });
        if (token.token != -1){
            setToken(token);
            navigate("/projetos");
        }
      }

    return (
        <div className="row justify-content-center mt-5">
            <div className="col-md-6">
                <h1 className="titulo-fino">Login</h1>
                <form onSubmit={handleSubmit}>
                    <div className="my-3">
                        <label className="form-label">Endereço de Email</label>
                        <input type="email" className="form-control" id="emailLogin" placeholder="name@example.com" onChange={e => setEmail(e.target.value)}></input>
                    </div>
                    <div className="mb-3">
                        <label className="form-label">Senha</label>
                        <input type="password" className="form-control" id="senhaLogin" onChange={e => setPassword(e.target.value)}></input>
                    </div>
                    <button className="btn btn-primary">Fazer Login</button>
                </form>
            </div>
        </div>
    );
}

Login.propTypes = {
    setToken: PropTypes.func.isRequired
}
export default Login;
