import { useEffect } from "react";
import { useState } from "react";
import CardProjeto from "../CardProjeto/CardProjeto";
import BotaoNovoProjeto from "../BotaoNovoProjeto/BotaoNovoProjeto";
import useToken from "../useToken";

function MeusProjetos() {

    const [projetos, setProjetos] = useState([]);
    const [ciclo, setCiclo] = useState([]);
    const { token, setToken } = useToken();

    useEffect(() => {
        fetch("http://localhost:8080/usuario/"+token+"/projetos",{
            method: "GET"
        })
            .then((res) => {
                return res.json();
            })
            .then((data) => {
                console.log(data);
                setProjetos(data);
            });
    }, []);

    return (
        <div className="row mt-5 justify-content-center">
            <div className="col-md-9 text-center mb-3">
                <BotaoNovoProjeto/>
            </div>

            <div className="col-md-9">

                {
                    projetos.map((projeto) => (
                        <CardProjeto imgsrc={projeto.foto?.caminho} key={projeto.id} nome={projeto.titulo} descr={projeto.descricao} ciclo={projeto.ciclo?.numero} time={projeto.dataDeModificacao} id={projeto.id} repositorio={projeto.repositorio} edit={true}/>
                    ))
                }
            </div>
                
        </div>
    );
}

export default MeusProjetos;
