import { useLocation } from 'react-router-dom';
import { useEffect, useState } from "react";
import Comentario from "../Comentario/Comentario";
import ModalC from "../ModalC/ModalC";
import timeFromGivenDate from '../auxiliares.js';
import './Projeto.css';

function Tutorial() {
    const { search } = useLocation();
    const match = search.match(/id=(.*)/);
    const id = match?.[1];

    const [tutorial, setTutorial] = useState([]);
    const [comentarios, setComentario] = useState([]);
    // const [grupo, setGrupo] = useState([]);

    useEffect(() => {
        fetch("http://localhost:8080/post/"+id,{
            method: "GET"
        })
            .then((res) => {
                return res.json();
            })
            .then((data) => {
                console.log(data);
                // data.caminhofoto = data.foto.caminho
                setTutorial(data);
                return data;
            })
            .then ( (projeto) => {

                fetch("http://localhost:8080/projeto/"+projeto.id+"/comentarios",{
                    method: "GET"
                })
                .then((res) => {
                    return res.json();
                })
                .then((data) => {
                    console.log(data);
                    setComentario(data);
                });
            })

    }, []);

    return (
        <>
            <div className="row mt-5 justify-content-center">
                <div className="col-md-8">
                    <h1 className="titulo-fino">
                        {tutorial.titulo}
                    </h1>
                    {/* <img className="img-cover imagemProjeto mt-3" src={tutorial.caminhofoto} alt=""></img> */}

                        <div className="my-4">
                            <p className="icon-link mx-2">
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-people-fill" viewBox="0 0 16 16">
                                <path d="M7 14s-1 0-1-1 1-4 5-4 5 3 5 4-1 1-1 1zm4-6a3 3 0 1 0 0-6 3 3 0 0 0 0 6m-5.784 6A2.24 2.24 0 0 1 5 13c0-1.355.68-2.75 1.936-3.72A6.3 6.3 0 0 0 5 9c-4 0-5 3-5 4s1 1 1 1zM4.5 8a2.5 2.5 0 1 0 0-5 2.5 2.5 0 0 0 0 5"/>
                                </svg>
                                Grupo: {tutorial.idGrupo}
                            </p>
                            <p className="icon-link mx-2" href="#">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-clock-history" viewBox="0 0 16 16">
                            <path d="M8.515 1.019A7 7 0 0 0 8 1V0a8 8 0 0 1 .589.022zm2.004.45a7 7 0 0 0-.985-.299l.219-.976q.576.129 1.126.342zm1.37.71a7 7 0 0 0-.439-.27l.493-.87a8 8 0 0 1 .979.654l-.615.789a7 7 0 0 0-.418-.302zm1.834 1.79a7 7 0 0 0-.653-.796l.724-.69q.406.429.747.91zm.744 1.352a7 7 0 0 0-.214-.468l.893-.45a8 8 0 0 1 .45 1.088l-.95.313a7 7 0 0 0-.179-.483m.53 2.507a7 7 0 0 0-.1-1.025l.985-.17q.1.58.116 1.17zm-.131 1.538q.05-.254.081-.51l.993.123a8 8 0 0 1-.23 1.155l-.964-.267q.069-.247.12-.501m-.952 2.379q.276-.436.486-.908l.914.405q-.24.54-.555 1.038zm-.964 1.205q.183-.183.35-.378l.758.653a8 8 0 0 1-.401.432z"/>
                            <path d="M8 1a7 7 0 1 0 4.95 11.95l.707.707A8.001 8.001 0 1 1 8 0z"/>
                            <path d="M7.5 3a.5.5 0 0 1 .5.5v5.21l3.248 1.856a.5.5 0 0 1-.496.868l-3.5-2A.5.5 0 0 1 7 9V3.5a.5.5 0 0 1 .5-.5"/>
                            </svg>
                                Última atualização: {timeFromGivenDate(tutorial.dataDeModificacao)}
                            </p>

                        </div>

                        <p>
                            {tutorial.descricao}
                        </p>
                </div>
            </div>


            <div className="row mt-5 justify-content-center">
                <div className="col-md-8">
                    <h2 className="titulo-fino">
                        Comentários
                    </h2>

                    <button className="btn btn-outline-primary" type="button" data-bs-toggle="modal" data-bs-target="#novoComentario">Novo Comentário</button>

                    <ModalC idComentarioPai={null} idConteudo={id} tipoConteudo="projeto" titulo="Novo Comentário" id="novoComentario"/>
                    
                    {
                        comentarios.map((comentario) => (
                            comentario.map( (comentario) => (
                                <Comentario nome={comentario.usuario} texto={comentario.texto}/>
                            ))
                        ))
                    }
                    
                </div>
            </div>
        </>
    );
}

export default Tutorial;
