import { Outlet, Link } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap/dist/js/bootstrap.min.js";
import useToken from '../useToken';


const Layout = () => {
  const { token, setToken } = useToken();
  return (
    <>
    <nav className="navbar navbar-expand-lg bg-dark  " data-bs-theme="dark">
      <div className="container ">
        <Link className="navbar-brand" to="/"><img width="100px" src="/imgs/logo_sf.png"></img></Link>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarNavAltMarkup">
          <div className="navbar-nav me-auto">
              <Link className="nav-link active" aria-current="page" to="/">Home</Link>
              <Link className="nav-link" to="/projetos">Projetos</Link>
              {token && <Link className="nav-link" to="/meusprojetos">Meus Projetos</Link>}
              <Link className="nav-link" to="/tutoriais">Tutoriais</Link>
              {token && <Link className="nav-link" to="/meustutoriais">Meus Tutoriais</Link>}
              {token && <Link className="nav-link" to="/meusgrupos">Meus Grupos</Link>}
              <Link className="nav-link" to="/perfil">Minha Conta</Link>
          </div>
          <div className="d-flex">
            {!token && <Link className="nav-link" to="/Register"><button className="btn btn-primary">Criar Conta</button></Link>}
            {!token && <Link className="nav-link" to="/Login"><button className="btn btn-primary mx-2">Login</button></Link>}
            {token && <button className="btn btn-outline-primary" onClick={() => { localStorage.clear(); window.location.reload(); }}>Logout</button>}
          </div>
        </div>
      </div>
    </nav>
    <div className="container">
      <Outlet />
    </div>
    </>
  )
};

export default Layout;
