import { useEffect } from "react";
import { useState } from "react";
import CardTutorial from "../CardTutorial/CardTutorial";
import BotaoNovoTutorial from "../BotaoNovoTutorial/BotaoNovoTutorial";

function Tutoriais() {

    const [tutoriais, setTutoriais] = useState([]);

    useEffect(() => {
        fetch("http://localhost:8080/posts",{
            method: "GET"
        })
        .then((res) => {
            return res.json();
        })
        .then((data) => {
            setTutoriais(data);
        });
                
    }, []);
    
    if (tutoriais.length > 0){
        return (
            <div className="row mt-5 justify-content-center">
                <div className="col-md-9">
                    {
                        tutoriais.map((tutorial) => (
                            <CardTutorial imgsrc={tutorial.foto?.caminho} key={tutorial.id} nome={tutorial.titulo} descr={tutorial.descricao} ciclo={tutorial.ciclo?.numero} time={tutorial.dataDeModificacao} id={tutorial.id} repositorio={tutorial.repositorio}/>
                        ))
                    }
                </div>
            </div>
        );
    } else{
        return (
            <div className="row mt-5 justify-content-center">
                <div className="col-md-9 text-center">
                    <BotaoNovoTutorial/>
                </div>
            </div>
        );
    }
}

export default Tutoriais;
