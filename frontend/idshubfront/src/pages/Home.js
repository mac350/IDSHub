import Banner from "../Banner/Banner";
import { Link } from "react-router-dom";

function Home() {
  return (
    <div class="container">
        <Banner />
        <div class="row justify-content-center mb-5">
            <div class="col-md-5 mt-3">
                <h2 class="titulo-fino">Criar um projeto</h2>
                <p>
                    Criar um projeto no IDSHub é muito fácil. É só clicar no botão abaixo, preencher algumas informações sobre o seu projeto e pronto!
                </p>
                <Link to="/meusprojetos"><button class="btn btn-outline-primary">Criar Projeto</button></Link>
            </div>
            <div class="col-md-5 mt-3">
                <h2 class="titulo-fino">Criar um Tutorial</h2>
                <p>
                    Criar um tutorial no IDSHub é muito fácil. É só clicar no botão abaixo, preencher algumas informações sobre o seu tutorial e pronto!
                </p>
                <Link to="/meusprojetos"><button class="btn btn-outline-primary">Criar Tutorial</button></Link>
            </div>
        </div>
    </div>
  );
}

export default Home;
