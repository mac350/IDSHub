import { useEffect } from "react";
import { useState } from "react";
import ListaProjeto from "../ListaProjeto/ListaProjeto";
import CardProjeto from "../CardProjeto/CardProjeto";
import BotaoNovoProjeto from "../BotaoNovoProjeto/BotaoNovoProjeto";
import Projeto from "./Projeto";

function Projetos() {

    const [projetos, setProjetos] = useState([]);
    const [viewMode, setViewMode] = useState('card'); // 'card' or 'list'

    useEffect(() => {
        fetch("http://localhost:8080/projetos",{
            method: "GET"
        })
        .then((res) => {
            return res.json();
        })
        .then((data) => {
            setProjetos(data);
        });
                
    }, []);
    
    if (projetos.length > 0){
        return (
            <>
            <div className="row mt-5 justify-content-center">
                <div className={viewMode === 'card' && "col-md-9" || "col-md-10"} >
                <div className="row justify-content-end mb-3">
                    <div className="col-auto">
                        <p className="btn btn-outline-primary" onClick={() => setViewMode('card')}>Card View</p>
                        <p className="btn btn-outline-primary" onClick={() => setViewMode('list')}>List View</p>
                    </div>
                </div>
                {projetos.map(projeto => (
                viewMode === 'card' ? (
                            <CardProjeto imgsrc={projeto.foto?.caminho} key={projeto.id} nome={projeto.titulo} descr={projeto.descricao} ciclo={projeto.ciclo?.numero} time={projeto.dataDeModificacao} id={projeto.id} repositorio={projeto.repositorio} grupo={projeto.idGrupo}/>
                        ) : (
                            <ListaProjeto imgsrc={projeto.foto?.caminho} key={projeto.id} nome={projeto.titulo} descr={projeto.descricao} ciclo={projeto.ciclo?.numero} time={projeto.dataDeModificacao} id={projeto.id} repositorio={projeto.repositorio} grupo={projeto.idGrupo}/>
                        )
                    ))}
                </div>
            </div>
            </>
        );
    } else{
        return (
            <div className="row mt-5 justify-content-center">
                <div className="col-md-9 text-center">
                    <BotaoNovoProjeto/>
                </div>
            </div>
        );
    }
}

export default Projetos;
