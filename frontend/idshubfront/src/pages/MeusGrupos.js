import { useEffect } from "react";
import { useState } from "react";
import CardGrupo from "../CardGrupo/CardGrupo";
import CardNovoItem from "../BotaoNovoProjeto/BotaoNovoProjeto";
import useToken from "../useToken";
import { Link } from 'react-router-dom';
import SelectGroup from "../SelectGroup/SelectGroup";

function Projetos() {

    const [grupos, setGrupos] = useState([]);
    const { token, setToken } = useToken();

    useEffect(() => {
        fetch("http://localhost:8080/usuario/" + token + "/grupos", {
            method: "GET"
        })
            .then((res) => {
                return res.json();
            })
            .then((data) => {
                console.log(data);
                setGrupos(data);
            });
    }, []);

    return (
        <div className="row mt-5 justify-content-center">
            <div className="col-md-9 text-center mb-3">
                <SelectGroup />
            </div>

            <div className="col-md-9">

                {
                    grupos.map((grupo) => (
                        <CardGrupo imgsrc={grupo.foto?.caminho} key={grupo.id} nome={grupo.nome} id={grupo.id} />
                    ))
                }
            </div>

        </div>
    );
}

export default Projetos;
