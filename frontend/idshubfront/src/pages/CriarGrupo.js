import { useLocation, useNavigate } from 'react-router-dom';
import React, { useState } from 'react';
import useToken from '../useToken';



function Register() {

    const navigate = useNavigate();
    const [nome, setNome] = useState('');
    const [foto, setFoto] = useState('');
    const { token, setToken } = useToken();

    const handleSubmit = (event) => {
        event.preventDefault();

        var novaImg = (document.getElementById("foto").value);

        const data = {
            nome: nome,
            idUsuarioCriador: token,
            caminhoFoto: novaImg
        };

        fetch("http://localhost:8080/grupos", {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        })
            .then(response => {
                if (response.ok) {
                    console.log('Dados enviados com sucesso!');
                } else {
                    console.error('Erro ao enviar os dados.');
                }
            })
            .catch(error => {
                console.error('Erro ao enviar os dados:', error);
            }).then(()=>{
                navigate("/meusgrupos");
            });
    };

    return (
        <div className="row justify-content-center mt-5">
            <div className="col-md-6">
                <h1 className="titulo-fino">Criar Grupo</h1>

                <form onSubmit={handleSubmit}>
                    <div className="mb-3">
                        <label htmlFor="exampleFormControlInput1" className="form-label">Nome do Grupo</label>
                        <input
                            type="text" className="form-control" id="nomeLogin" name="nomeLogin" placeholder="nome_do_grupo"
                            value={nome}
                            onChange={(event) => setNome(event.target.value)}
                        />
                    </div>
                    <div className="mb-3">
                        <label htmlFor="exampleFormControlInput1" className="form-label">Endereço para foto do grupo</label>
                        <input
                            type="text"
                            className="form-control" id="foto" name="foto" placeholder="https://google.com/minhafoto.png"
                            value={foto}
                            onChange={(event) => setFoto(event.target.value)}
                        />
                    </div>
                    <button type="submit" className="btn btn-primary">Criar Grupo</button>
                </form>
            </div>
        </div>
    );
}

export default Register;

