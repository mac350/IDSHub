import { Outlet, Link } from "react-router-dom";
import { useEffect, useState } from "react";
import './Perfil.css';

function EditarPerfil() {
    
    const [usuario, setUsuario] = useState([]);

    var id = JSON.parse(localStorage.getItem("token")).token;
    useEffect(() => {
        fetch("http://localhost:8080/usuario/"+id, {
            method: "GET"
        })
            .then((res) => {
                return res.json();
            })
            .then((data) => {
                data.foto = data.foto.caminho;
                setUsuario(data);
            });

    }, []);

    const mudaAtributo = (atributo, novoAtributo) =>{
        fetch("http://localhost:8080/usuario/"+id+"?atributo="+atributo, {
                method: 'PUT',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: novoAtributo
            })
            .then(response => {
                if (response.ok) {
                    console.log('Dados enviados com sucesso!');
                } else {
                    console.error('Erro ao enviar os dados.');
                }
            })
            .catch(error => {
                console.error('Erro ao enviar os dados:', error);
            }).then(()=>{
                window.location.reload();
            })
    }

    const handleSubmit = (event) => {
        event.preventDefault();

        var novoNome = (document.getElementById("nomeInput").value);
        var novoEmail = (document.getElementById("emailInput").value);
        var novaImagem = (document.getElementById("imgInput").value);
        var novaSenha = (document.getElementById("senhaInput").value);

        if (novoNome != "" && novoNome != null){
            mudaAtributo("nome", novoNome);
        }

        if (novaImagem != "" && novaImagem != null){
            mudaAtributo("foto", novaImagem);
        }

        if (novaSenha != "" && novaSenha != null){
            mudaAtributo("senha", novaSenha);
        }

        if (novoEmail != "" && novoEmail != null){
            mudaAtributo("email", novoEmail);
        }

    };
    
    return (
        <>
            <div className="row mt-5 justify-content-center">
                <div className="col-9">
                    <div className="row justify-content-center mt-5">
                        <div className="col-auto">
                            <img src={usuario.foto} className="imagemPerfil"></img>
                        </div>
                        <div className="col-auto">
                            <h2 className="mt-md-4">{usuario.nome}</h2>
                            <p className='lead'>{usuario.email}</p>
                            <Link to="/editarPerfil"><button className='btn btn-primary'>Editar Perfil</button></Link>
                            <button className='mx-2 btn btn-outline-primary'>Mudar Senha</button>
                        </div>
                    </div>
                    <div className="row justify-content-center my-5">
                        <div className="col-6">
                            <form action>
                                <h2>Fazer alterações:</h2>
                                <input id="nomeInput" className="form-control" placeholder="Novo Nome"/>
                                <input id="emailInput" className="form-control mt-2" placeholder="Novo Email"/>
                                <input id="imgInput" className="form-control mt-2" placeholder="Nova Img"/>
                                <input id="senhaInput" className="form-control mt-2" placeholder="Nova Senha"/>
                                <button className='mt-2 btn btn-primary' onClick={handleSubmit}>Salvar Mudanças</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
}

export default EditarPerfil ;
