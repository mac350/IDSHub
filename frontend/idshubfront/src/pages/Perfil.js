import { Outlet, Link } from "react-router-dom";
import { useEffect, useState } from "react";
import './Perfil.css';

function Perfil() {
    
    const [usuario, setUsuario] = useState([]);

    useEffect(() => {
        var id = JSON.parse(localStorage.getItem("token")).token;
        fetch("http://localhost:8080/usuario/"+id, {
            method: "GET"
        })
            .then((res) => {
                return res.json();
            })
            .then((data) => {
                console.log(data);
                data.foto = data.foto.caminho;
                setUsuario(data);

            });

    }, []);
    
    return (
        <>
            <div className="row mt-5 justify-content-center">
                <div className="col-9">
                    <div className="row justify-content-center mt-5">
                        <div className="col-auto">
                            <div className="imgBack">
                                <img src={(usuario.foto)} className="imagemPerfil"></img>  
                            </div>
                        </div>
                        <div className="col-auto">
                            <h2 className="mt-md-4">{usuario.nome}</h2>
                            <p className='lead'>{usuario.email}</p>
                            <Link to="/editarPerfil"><button className='btn btn-primary'>Editar Perfil</button></Link>
                            <button className='mx-2 btn btn-outline-primary'>Mudar Senha</button>
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
}

export default Perfil;
