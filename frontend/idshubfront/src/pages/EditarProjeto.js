import { useLocation, useNavigate } from 'react-router-dom';
import { useEffect, useState } from "react";
import Comentario from "../Comentario/Comentario";
import ModalC from "../ModalC/ModalC";
import Modal from "../Modal/Modal";
import timeFromGivenDate from '../auxiliares.js';
import './Projeto.css';

function EditarProjeto() {
    const navigate = useNavigate();
    const { search } = useLocation();
    const match = search.match(/id=(.*)/);
    const id = match?.[1];

    const [projeto, setProjeto] = useState([]);
    const [comentarios, setComentario] = useState([]);

    const apagarProjeto = () => {
        fetch("http://localhost:8080/projeto/"+projeto.id, {
            method: 'DELETE'
        })
            .then(response => {
                if (response.ok) {
                    console.log('Projeto apagado com sucesso!');
                } else {
                    console.error('Erro ao apagar projeto.');
                }
            })
            .catch(error => {
                console.error('Erro ao enviar os dados:', error);
            }).then(() => {
                navigate("/meusprojetos");
            })
        }

    useEffect(() => {
            fetch("http://localhost:8080/projeto/" + id, {
                method: "GET"
            })
            .then((res) => {
                return res.json();
            })
            .then((data) => {
                data.caminhofoto = data.foto.caminho
                data.numCiclo = data.ciclo.numero
                setProjeto(data);
                return data;
            }).then ( (projeto) => {

                fetch("http://localhost:8080/projeto/"+projeto.id+"/comentarios",{
                    method: "GET"
                })
                .then((res) => {
                    return res.json();
                })
                .then((data) => {
                    console.log(data);
                    setComentario(data);
                });
            })
    }, []);

    return (
        <>
            <div className="row mt-5 justify-content-center">
                <div className="col-md-8">
                    <div className="d-flex justify-content-between">
                        <h1 className="titulo-fino">
                            {projeto.titulo}
                        </h1>

                        <button className="btn btn-outline-primary" type="button" data-bs-toggle="modal" data-bs-target="#editarTitulo">Editar Título</button>

                        <Modal idProjeto={id} atributo="titulo" titulo="Editar Título" id="editarTitulo"/>
                    </div>
                    <img className="img-cover imagemProjeto my-3" src={projeto.caminhofoto} alt=""></img>

                    <button className="btn btn-outline-primary" type="button" data-bs-toggle="modal" data-bs-target="#editarFoto">Editar Imagem</button>

                        <Modal idProjeto={id} atributo="foto" titulo="Editar Foto" id="editarFoto"/>

                    <div className="d-flex justify-content-between my-3">
                        <h2 className="titulo-fino">
                            Informações gerais:
                        </h2>
                        <button className="btn btn-outline-primary" type="button" data-bs-toggle="modal" data-bs-target="#editarRepositorio">Editar Repositorio</button>
                        <Modal idProjeto={id} link={"http://localhost:8080/projeto/"+id+"?atributo=repositorio"} atributo="repositorio" titulo="Editar Repositorio" id="editarRepositorio"/>
                        <button className="btn btn-outline-primary" type="button" data-bs-toggle="modal" data-bs-target="#editarCiclo">Editar Ciclo</button>
                        <Modal idProjeto={id} atributo="ciclo" titulo="Editar Ciclo" id="editarCiclo"/>
                    </div>

                    <div className="my-4">
                            <p className="icon-link mx-2">
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-people-fill" viewBox="0 0 16 16">
                                <path d="M7 14s-1 0-1-1 1-4 5-4 5 3 5 4-1 1-1 1zm4-6a3 3 0 1 0 0-6 3 3 0 0 0 0 6m-5.784 6A2.24 2.24 0 0 1 5 13c0-1.355.68-2.75 1.936-3.72A6.3 6.3 0 0 0 5 9c-4 0-5 3-5 4s1 1 1 1zM4.5 8a2.5 2.5 0 1 0 0-5 2.5 2.5 0 0 0 0 5"/>
                                </svg>
                                Grupo: {projeto.idGrupo}
                            </p>
                            <p className="icon-link mx-2" href="#">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-clock-history" viewBox="0 0 16 16">
                            <path d="M8.515 1.019A7 7 0 0 0 8 1V0a8 8 0 0 1 .589.022zm2.004.45a7 7 0 0 0-.985-.299l.219-.976q.576.129 1.126.342zm1.37.71a7 7 0 0 0-.439-.27l.493-.87a8 8 0 0 1 .979.654l-.615.789a7 7 0 0 0-.418-.302zm1.834 1.79a7 7 0 0 0-.653-.796l.724-.69q.406.429.747.91zm.744 1.352a7 7 0 0 0-.214-.468l.893-.45a8 8 0 0 1 .45 1.088l-.95.313a7 7 0 0 0-.179-.483m.53 2.507a7 7 0 0 0-.1-1.025l.985-.17q.1.58.116 1.17zm-.131 1.538q.05-.254.081-.51l.993.123a8 8 0 0 1-.23 1.155l-.964-.267q.069-.247.12-.501m-.952 2.379q.276-.436.486-.908l.914.405q-.24.54-.555 1.038zm-.964 1.205q.183-.183.35-.378l.758.653a8 8 0 0 1-.401.432z"/>
                            <path d="M8 1a7 7 0 1 0 4.95 11.95l.707.707A8.001 8.001 0 1 1 8 0z"/>
                            <path d="M7.5 3a.5.5 0 0 1 .5.5v5.21l3.248 1.856a.5.5 0 0 1-.496.868l-3.5-2A.5.5 0 0 1 7 9V3.5a.5.5 0 0 1 .5-.5"/>
                            </svg>
                                Última atualização: {timeFromGivenDate(projeto.dataDeModificacao)}
                            </p>
                            <p className="icon-link mx-2" href="#">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-arrow-repeat" viewBox="0 0 16 16">
                            <path d="M11.534 7h3.932a.25.25 0 0 1 .192.41l-1.966 2.36a.25.25 0 0 1-.384 0l-1.966-2.36a.25.25 0 0 1 .192-.41m-11 2h3.932a.25.25 0 0 0 .192-.41L2.692 6.23a.25.25 0 0 0-.384 0L.342 8.59A.25.25 0 0 0 .534 9"/>
                            <path fill-rule="evenodd" d="M8 3c-1.552 0-2.94.707-3.857 1.818a.5.5 0 1 1-.771-.636A6.002 6.002 0 0 1 13.917 7H12.9A5 5 0 0 0 8 3M3.1 9a5.002 5.002 0 0 0 8.757 2.182.5.5 0 1 1 .771.636A6.002 6.002 0 0 1 2.083 9z"/>
                            </svg>
                                Ciclo Atual: {projeto.numCiclo}
                            </p>
                            <a className="icon-link mx-2" href={projeto.repositorio}>
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-git" viewBox="0 0 16 16">
                                <path d="M15.698 7.287 8.712.302a1.03 1.03 0 0 0-1.457 0l-1.45 1.45 1.84 1.84a1.223 1.223 0 0 1 1.55 1.56l1.773 1.774a1.224 1.224 0 0 1 1.267 2.025 1.226 1.226 0 0 1-2.002-1.334L8.58 5.963v4.353a1.226 1.226 0 1 1-1.008-.036V5.887a1.226 1.226 0 0 1-.666-1.608L5.093 2.465l-4.79 4.79a1.03 1.03 0 0 0 0 1.457l6.986 6.986a1.03 1.03 0 0 0 1.457 0l6.953-6.953a1.03 1.03 0 0 0 0-1.457"/>
                                </svg>
                                Ver Repositório
                            </a>
                        </div>

                    <div className="d-flex justify-content-between my-3">

                        <h2 className="titulo-fino">
                            Sobre o projeto:
                        </h2>
                        <button className="btn btn-outline-primary" type="button" data-bs-toggle="modal" data-bs-target="#editarDescricao">Editar Descrição</button>
                        <Modal idProjeto={id} type="textarea" atributo="descricao" titulo="Editar Descrição" id="editarDescricao"/>
                    </div>
                    <p id="texto">
                        {projeto.descricao}
                    </p>
                </div>
            </div>

            <div className="row mt-5 justify-content-center">
                <div className="col-md-8">
                    <button className="btn btn-danger" type="button" onClick={apagarProjeto}>Apagar Projeto</button>
                </div>
            </div>



            <div className="row mt-5 justify-content-center">
                <div className="col-md-8 mb-5">
                    <h2 className="titulo-fino">
                        Comentários
                    </h2>

                    <button className="btn btn-outline-primary" type="button" data-bs-toggle="modal" data-bs-target="#novoComentario">Novo Comentário</button>

                    <ModalC idComentarioPai={null} idConteudo={id} tipoConteudo="projeto" titulo="Novo Comentário" id="novoComentario"/>
                    
                    {
                        comentarios.map((comentario) => (
                            comentario.map( (comentario) => (
                                <Comentario nome={comentario.usuario} texto={comentario.texto}/>
                            ))
                        ))
                    }
                    
                </div>
            </div>
        </>
    );
}

export default EditarProjeto;
