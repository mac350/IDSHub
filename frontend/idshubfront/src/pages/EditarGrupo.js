import { useLocation, useNavigate } from 'react-router-dom';
import { useEffect, useState } from "react";
import useToken from "../useToken";

function EditarGrupo(props) {
    const navigate = useNavigate();
    const { search } = useLocation();
    const match = search.match(/id=(.*)/);
    const id = match?.[1];

    const [grupo, setGrupo] = useState([]);
    const [integrantes, setIntegrantes] = useState([]);
    const [solicitacoes, setSolicitacoes] = useState([]);
    // const [idUsuario, setIdUsuario] = useState([]);

    const { token, setToken } = useToken();
    
    const pegarIntegrantes = () => {
        fetch("http://localhost:8080/grupo/"+id+"/usuarios",{
            method: "GET"
        })
        .then((res) => {
            return res.json();
        })
        .then((data) => {
            console.log(data);
            setIntegrantes(data);
        });
    };

    const pegarSolicitacoes = () => {
        fetch("http://localhost:8080/grupo/"+id+"/solicitacoes",{
            method: "GET"
        })
        .then((res) => {
            return res.json();
        })
        .then((data) => {
            console.log(data);
            setSolicitacoes(data);
        });
    };

    const mudaAtributo = (atributo, novoAtributo) =>{
        fetch("http://localhost:8080/grupo/"+id+"?atributo="+atributo, {
                method: 'PUT',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: novoAtributo
            })
            .then(response => {
                if (response.ok) {
                    console.log('Dados enviados com sucesso!');
                } else {
                    console.error('Erro ao enviar os dados.');
                }
            })
            .catch(error => {
                console.error('Erro ao enviar os dados:', error);
            }).then(()=>{
                window.location.reload();
            })
    }

    const handleSubmit = (event) => {
        event.preventDefault();

        var novoNome = (document.getElementById("nomeInput").value);
        var novaImagem = (document.getElementById("imgInput").value);

        if (novoNome != "" && novoNome != null){
            mudaAtributo("nome", novoNome);
        }

        if (novaImagem != "" && novaImagem != null){
            mudaAtributo("foto", novaImagem);
        }

    };

    const sairDoGrupo = (idUsuario) => {

        const data = {
            idUsuario: idUsuario,
            idGrupo: id
        };

        console.log(data);

        fetch("http://localhost:8080/sairGrupo", {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        })
            .then(response => {
                if (response.ok) {
                    console.log('Dados enviados com sucesso!');
                    window.location.replace("/");
                } else {
                    console.error('Erro ao enviar os dados.');
                }
            })
            .catch(error => {
                console.error('Erro ao enviar os dados:', error);
            });
    };

    const aceitarSolicitacao = (idUsuario) => {

        const data = {
            idUsuario: idUsuario,
            idGrupo: id
        };

        console.log(data);

        fetch("http://localhost:8080/aceitarSolicitacao", {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        })
            .then(response => {
                if (response.ok) {
                    console.log('Dados enviados com sucesso!');
                    window.location.reload();
                } else {
                    console.error('Erro ao enviar os dados.');
                }
            })
            .catch(error => {
                console.error('Erro ao enviar os dados:', error);
            });
    };

    const rejeitarSolicitacao = (idUsuario) => {

        const data = {
            idUsuario: idUsuario,
            idGrupo: id
        };

        console.log(data);

        fetch("http://localhost:8080/rejeitarSolicitacao", {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        })
            .then(response => {
                if (response.ok) {
                    console.log('Dados enviados com sucesso!');
                    window.location.reload();
                } else {
                    console.error('Erro ao enviar os dados.');
                }
            })
            .catch(error => {
                console.error('Erro ao enviar os dados:', error);
            });
    };

    useEffect(() => {
        fetch("http://localhost:8080/grupo/" + id, {
            method: "GET"
        })
        .then((res) => {
            return res.json();
        })
        .then((data) => {
            setGrupo(data);
            pegarIntegrantes();
            pegarSolicitacoes();
        });    
    }, []);

   
    return (
        <>
            <div className="row mt-5 justify-content-center">
                <div className="col-md-8">
                    <h1>Editar Grupo</h1>
                    <input className=' my-3 form-control' placeholder='Novo Nome' id="nomeInput"/>
                    <input className=' my-3 form-control' placeholder='Nova Foto' id="imgInput"/>
                    <button className='btn btn-primary' onClick={handleSubmit}>Salvar</button>
                    <button className='btn btn-outline-danger mx-2' onClick={() => sairDoGrupo(token)}>Sair do Grupo</button>
                    <h2 className='mt-3'>Usuários</h2>
                    {
                        integrantes.map((integrante) => (
                            <p key="integrante?.id">{integrante?.nome + " "}</p>
                        ))
                    }
                    <h2 className='mt-3'>Solicitações</h2>
                    {
                        solicitacoes.map((solicitacao) => (
                            <p key="solicitacao.id">{solicitacao.nome} <button idusuario={solicitacao.id} className='mx-3 btn btn-success btn-sm' onClick={() => aceitarSolicitacao(solicitacao.id)}>Aceitar</button><button idusuario={solicitacao.id}  className='btn btn-danger btn-sm' onClick={() => rejeitarSolicitacao(solicitacao.id)}>Remover</button></p>
                        ))
                    }
                </div>
            </div>
        </>
    );
}

export default EditarGrupo;
