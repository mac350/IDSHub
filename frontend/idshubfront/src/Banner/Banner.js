import './Banner.css';
import { Link } from "react-router-dom";

function Banner() {
  return (
    <div className="row justify-content-center">
        <div className="col-md-9 px-md-5 mb-5 text-center">
            <img src="../imgs/logo_dark.png" className="img-fluid"></img>
            <Link to="/projetos"><button className="btn btn-primary">Ver Projetos</button></Link>
        </div>
    </div>
  );
}

export default Banner;
