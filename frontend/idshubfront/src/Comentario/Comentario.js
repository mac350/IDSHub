import { Link } from 'react-router-dom';
import timeFromGivenDate from '../auxiliares.js';

function Comentario(props) {

    return (
        <div className="card my-3">
            <div className="card-body">
                <h5 className="card-title">{props.nome}</h5>
                <p className="card-text"> {props.texto}</p>
                <button className="mx-1 btn btn-outline-secondary" data-bs-toggle="modal" data-bs-target="#novoComentario"> Responder </button>
                <button className="mx-1 btn btn-outline-secondary"> Like </button>
                <button className="mx-1 btn btn-outline-secondary"> Dislike </button>
            </div>
        </div>
    );
}

export default Comentario;