import { useNavigate } from "react-router-dom";
import { useEffect } from "react";
import { useState } from "react";
import { Link } from 'react-router-dom';
import useToken from "../useToken";

function SelectGroup() {
    const [grupos, setGrupos] = useState([]);
    const { token, setToken } = useToken();

    const pegarGrupos = () => {
        fetch("http://localhost:8080/grupos",{
            method: "GET"
        })
        .then((res) => {
            return res.json();
        })
        .then((data) => {
            console.log(data);
            setGrupos(data);
        });
    };

    const handleSubmit = (event) => {
        event.preventDefault();
        var grupoId = document.getElementById("selectEl").value;

        const data = {
            idUsuario: token,
            idGrupo: grupoId
        };

        fetch("http://localhost:8080/fazerSolicitacao", {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        })
            .then(response => {
                if (response.ok) {
                    console.log('Dados enviados com sucesso!');
                } else {
                    console.error('Erro ao enviar os dados.');
                }
            })
            .catch(error => {
                console.error('Erro ao enviar os dados:', error);
            });
    };

    useEffect(() => {
        pegarGrupos();
    }, []);


    return (
        <div className="row">
            <div className="col-auto">
                <select id="selectEl" class="form-select" aria-label="Default select example">
                    <option selected>Open this select menu</option>
                    {
                        grupos.map((grupo) => (
                            <option value={grupo.id}>{grupo.nome}</option>
                        ))
                    }
                </select>
            </div>
            <div className="col-auto">
                <button onClick={handleSubmit} className="text-center mx-1 btn-primary btn">Enviar solicitacao</button>
                <Link to="/CriarGrupo"><button className="btn btn-primary">Novo Grupo</button></Link>
            </div>
        </div>
    );
}

export default SelectGroup;