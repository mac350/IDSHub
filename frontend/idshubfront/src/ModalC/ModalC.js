import { useEffect, useState } from "react";
import useToken from '../useToken';

function Modal(props) {

    const [texto, setTexto] = useState('');
    const { token, setToken } = useToken();

    const handleSubmit = (event) => {
        event.preventDefault();
        
        if (!token){
            window.location.replace("/login");
            return;
        }

        const data = {
            idUsuario: token,
            idComentarioPai: props.idComentarioPai,
            idConteudo: props.idConteudo,
            tipoConteudo: props.tipoConteudo,
            texto: texto
        };

        fetch("http://localhost:8080/comentarios", {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        })
        .then(response => {
            if (response.ok) {
                console.log('Dados enviados com sucesso!');
                window.location.reload();
            } else {
                console.error('Erro ao enviar os dados.');
            }
        })
        .catch(error => {
            console.error('Erro ao enviar os dados:', error);
        });
    };
        
    
    return (
        <div className="modal fade" id={props.id} tabIndex="-1">
            <div className="modal-dialog">
                <div className="modal-content">
                <div className="modal-header">
                    <h1 className="modal-title fs-5">Novo Comentário</h1>
                    <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div className="modal-body">
                    <textarea onChange={(event) => setTexto(event.target.value)} type="text-area" placeholder="Digite aqui o novo comentário" className="form-control" rows="15"></textarea>
                </div>
                <div className="modal-footer">
                    <button type="button" className="btn btn-secondary" data-bs-dismiss="modal">Fechar</button>
                    <button type="button" onClick={handleSubmit} className="btn btn-primary" data-bs-dismiss="modal">Enviar Comentário</button>
                </div>
                </div>
            </div>
        </div>
    );
}

export default Modal;