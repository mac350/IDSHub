import './CardTutorial.css';
import { Link } from 'react-router-dom';
import timeFromGivenDate from '../auxiliares.js';

function CardTutorial(props) {
    if (props.edit){
        return (
            <div className="card mb-3">
                <img src={props.imgsrc} className="card-img-top img-cover" height="150px"></img>
                    <div className="card-body">
                        <h5 className="card-title">{props.nome}</h5>
                        <p className="card-text descr-projeto">{props.descr}</p>
                        <p className="card-text"><small className="text-body-secondary">Última atualização {timeFromGivenDate(props.time)} </small></p>
                        <Link to={'/Tutorial?id=' + props.id}><button className="btn-primary btn">Ver Tutorial</button></Link>
                        <Link className="mx-1 br"to={'/editarTutorial?id=' + props.id}>Editar</Link>
                    </div>
            </div>
        );
    }

    return (
        <div className="card mb-3">
            <img src={props.imgsrc} className="card-img-top img-cover" height="150px"></img>
                <div className="card-body">
                    <h5 className="card-title">{props.nome}</h5>
                    <p className="card-text descr-projeto">{props.descr}</p>
                    <p className="card-text"><small className="text-body-secondary">Última atualização {timeFromGivenDate(props.time)} </small></p>
                    <Link to={'/Tutorial?id=' + props.id}><button className="btn-primary btn">Ver Tutorial</button></Link>
                </div>
        </div>
    );

    
}

export default CardTutorial;