import './CardGrupo.css';
import { Link } from 'react-router-dom';
import { useEffect } from "react";
import { useState } from "react";
import timeFromGivenDate from '../auxiliares.js';

function CardGrupo(props) {
    const [integrantes, setIntegrantes] = useState([]);

    useEffect(() => {
        fetch("http://localhost:8080/grupo/"+props.id+"/usuarios",{
            method: "GET"
        })
            .then((res) => {
                return res.json();
            })
            .then((data) => {
                console.log(data);
                setIntegrantes(data);
            });
    }, []);

    if (integrantes.length > 0 ){
        return (
            <div className="card mb-3">
                <div className='row g-0'>
                    <div className='col-md-2'>
                        <img src={props.imgsrc} className=" rounded-start img-cover img-fluid" height="100px"></img>
                    </div>
                    <div className='col-md-10'>
                <div className="card-body">
    
                        <h5 className="card-title">{props.nome}</h5>
                        
                        <small>{
                            integrantes.map((integrante) => (
                                integrante?.nome + ", "
                            ))
                        }</small>
                        <p className="card-text descr-projeto">{props.descr}</p>
                        <p><Link to={'/editargrupo?id=' + props.id}>Editar Grupo</Link></p>
                </div>
                    </div>
                </div>
            </div>
        );
    } else{
        return (<div></div>);
    }
}

export default CardGrupo;