import { Link } from 'react-router-dom';
import React, { useState, useEffect } from 'react';
import timeFromGivenDate from '../auxiliares.js';
import './ListaProjeto.css';


function ListaProjeto(props) {
    const [grupo, setGrupo] = useState([]);

    useEffect(() => {
        fetch("http://localhost:8080/grupo/"+props.grupo,{
            method: "GET"
        })
        .then((res) => {
            return res.json();
        })
        .then((data) => {
            setGrupo(data);
        });
                
    }, []);

    return (
        <div className="list-item mb-2">
            <div className="list-body row">
                <div className="col-lg-2 d-flex">
                    <span className="list-title">{props.nome}</span>
                </div>
                <div className="col-lg-3 d-flex">
                    <span className="list-text descr-projeto">{props.descr}</span>

                </div>
                <div className="col-lg-4 d-flex">
                    <span className="list-text">
                        <small className="text-body-secondary">
                            {timeFromGivenDate(props.time)} | Ciclo: {props.ciclo} | {grupo.nome}
                        </small>
                    </span>
                </div>
                <div className='col-lg-3 p-0'>
                        <Link to={'/Projeto?id=' + props.id} className="link-repo">
                            Ver projeto
                        </Link>

                        <a href={props.repositorio} className="link-repo">
                            Ver repositório
                        </a>

                        {props.edit && (
                            <Link className="link-edit" to={'/editarprojeto?id=' + props.id}>Editar</Link>
                        )}
                </div>
            </div>
        </div>
    );
}

export default ListaProjeto;
