import React, { useState } from 'react';

function Modal(props) {
    const [numero, setNumero] = useState('');
    const [inicio, setInicico] = useState('');
    const [fim, setFim] = useState('');

    const handleSubmit = (event) => {
        event.preventDefault();


        var novoTitulo = (document.getElementById(props.id+"input").value);

        if (props.atributo != "ciclo"){
            fetch("http://localhost:8080/projeto/"+props.idProjeto+"?atributo="+props.atributo, {
                    method: 'PUT',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: novoTitulo
            })
            .then(response => {
                if (response.ok) {
                    console.log('Dados enviados com sucesso!');
                    window.location.reload();
                } else {
                    console.error('Erro ao enviar os dados.');
                }
            })
            .catch(error => {
                console.error('Erro ao enviar os dados:', error);
            })

        } else{
            const data = {
                numero:numero,
                inicio:inicio,
                fim:fim
            }
            fetch("http://localhost:8080/projeto/"+props.idProjeto+"/ciclo", {
                method: 'PUT',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(data)
            })
            .then(response => {
                if (response.ok) {
                    console.log('Dados enviados com sucesso!');
                    window.location.reload();
                } else {
                    console.error('Erro ao enviar os dados.');
                }
            })
            .catch(error => {
                console.error('Erro ao enviar os dados:', error);
            })
        }
    };
        
    if (props.type == "textarea"){

        return (
            <div className="modal fade" id={props.id} tabIndex="-1">
                <div className="modal-dialog">
                    <div className="modal-content">
                    <div className="modal-header">
                        <h1 className="modal-title fs-5" id={props.labelId}>{props.titulo}</h1>
                        <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div className="modal-body">
                        <textarea id={props.id + "input"} type="text-area" placeholder="Digite aqui a nova descrição" className="form-control" rows="15">{props.texto}</textarea>
                    </div>
                    <div className="modal-footer">
                        <button type="button" className="btn btn-secondary" data-bs-dismiss="modal">Fechar</button>
                        <button type="button" onClick={handleSubmit} className="btn btn-primary" data-bs-dismiss="modal">Salvar Mudanças</button>
                    </div>
                    </div>
                </div>
            </div>
        );

    }
    else if (props.atributo == "ciclo"){
        return (
            <div className="modal fade" id={props.id} tabIndex="-1">
                <div className="modal-dialog">
                    <div className="modal-content">
                    <div className="modal-header">
                        <h1 className="modal-title fs-5" id={props.labelId}>{props.titulo}</h1>
                        <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div className="modal-body">
                        <p>Número do ciclo</p>
                        <input id={props.id + "input"} placeholder="..." type="number" className="form-control" onChange={(event) => setNumero(event.target.value)}/>
                    </div>
                    <div className="modal-body">
                        <p>Início do ciclo</p>
                        <input id={props.id + "input"} placeholder="..." type="date" className="form-control" onChange={(event) => setInicico(event.target.value)}/>
                    </div>
                    <div className="modal-body">
                        <p>Fim do ciclo</p>
                        <input id={props.id + "input"} placeholder="..." type="date" className="form-control" onChange={(event) => setFim(event.target.value)}/>
                    </div>
                    <div className="modal-footer">
                        <button type="button" className="btn btn-secondary" data-bs-dismiss="modal">Fechar</button>
                        <button type="button" onClick={handleSubmit} className="btn btn-primary" data-bs-dismiss="modal">Salvar Mudanças</button>
                    </div>
                    </div>
                </div>
            </div>
        );
    } else{
        return (
            <div className="modal fade" id={props.id} tabIndex="-1">
                <div className="modal-dialog">
                    <div className="modal-content">
                    <div className="modal-header">
                        <h1 className="modal-title fs-5" id={props.labelId}>{props.titulo}</h1>
                        <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div className="modal-body">
                        <input id={props.id + "input"} placeholder="..." type="text-area" className="form-control"/>
                    </div>
                    <div className="modal-footer">
                        <button type="button" className="btn btn-secondary" data-bs-dismiss="modal">Fechar</button>
                        <button type="button" onClick={handleSubmit} className="btn btn-primary" data-bs-dismiss="modal">Salvar Mudanças</button>
                    </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Modal;