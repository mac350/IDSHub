import './App.css';
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Layout from './pages/Layout';
import Home from './pages/Home';
import Login from './pages/Login';
import Register from './pages/Register';
import Projetos from './pages/Projetos';
import Projeto from './pages/Projeto';
import EditarProjeto from './pages/EditarProjeto';
import Tutoriais from './pages/Tutoriais';
import Tutorial from './pages/Tutorial';
import EditarTutorial from './pages/EditarTutorial';
import MeusProjetos from './pages/MeusProjetos';
import MeusTutoriais from './pages/MeusTutoriais';
import MeusGrupos from './pages/MeusGrupos';
import Perfil from './pages/Perfil';
import EditarPerfil from './pages/EditarPerfil';
import EditarGrupo from './pages/EditarGrupo';
import CriarGrupo from './pages/CriarGrupo';
import useToken from './useToken';

function App() {

    const { token, setToken } = useToken();

    if (!token) {
        return (
            <BrowserRouter>
                <Routes>
                    <Route path="/" element={<Layout />}>
                        <Route index element={<Home />} />
                        <Route path="/projetos" element={<Projetos />} />
                        <Route path="/projeto" element={<Projeto />} />
                        <Route path="/tutoriais" element={<Tutoriais />} />
                        <Route path="/tutorial" element={<Tutorial />} />
                        <Route path="/login" element={<Login setToken={setToken} />} />
                        <Route path="/register" element={<Register />} />
                        <Route path="*" element={<Login setToken={setToken} />} />
                    </Route>
                </Routes>
            </BrowserRouter>
        )
    }
    
    return (
        <div className="App">
            <BrowserRouter>
                <Routes>
                        <Route path="/" element={<Layout />}>
                        <Route path="/perfil" element={<Perfil />} />
                        <Route path="/editarperfil" element={<EditarPerfil />} />
                        <Route index element={<Home />} />
                        <Route path="/projetos" element={<Projetos />} />
                        <Route path="/projeto" element={<Projeto />} />
                        <Route path="/editarProjeto" element={<EditarProjeto />} />
                        <Route path="/tutoriais" element={<Tutoriais />} />
                        <Route path="/tutorial" element={<Tutorial />} />
                        <Route path="/editarTutorial" element={<EditarTutorial />} />
                        <Route path="/editarGrupo" element={<EditarGrupo />} />
                        <Route path="/criarGrupo" element={<CriarGrupo />} />
                        <Route path="/login" element={<Login setToken={setToken} />} />
                        <Route path="/register" element={<Register />} />
                        <Route path="/meusprojetos" element={<MeusProjetos />} />
                        <Route path="/MeusTutoriais" element={<MeusTutoriais />} />
                        <Route path="/MeusGrupos" element={<MeusGrupos />} />
                    </Route>
                </Routes>
            </BrowserRouter>
        </div>
    );
}

export default App;
