export default function timeFromGivenDate(dateString) {
    // Parse the given date string
    const givenDate = new Date(dateString);

    // Get the current date and time
    const currentDate = new Date();

    // Calculate the difference in time (in milliseconds)
    const timeDifference = currentDate - givenDate;

    // Convert the time difference to minutes, hours, and days
    const minutesDifference = timeDifference / (1000 * 60);
    const hoursDifference = timeDifference / (1000 * 60 * 60);
    const daysDifference = timeDifference / (1000 * 60 * 60 * 24);

    // Determine the appropriate unit and return the result
    if (daysDifference >= 1) {
        return `${Math.floor(daysDifference)} dias atrás`;
    } else if (hoursDifference >= 1) {
        return `${Math.floor(hoursDifference)} horas atrás`;
    } else {
        return `${Math.floor(minutesDifference)} minutos atrás`;
    }
}